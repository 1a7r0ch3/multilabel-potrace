# Multilabel Potrace

Extract and simplifies contours delimiting homogeneous connected components
within a 2D grid structure (typically, pixels of an image).

The assignment of each pixel to its component must be given. Then, this software allows to perform the following steps :
1. decompose the borders of each components into paths which are either closed, or connecting "triple points", which are points where three or more components are joining 
2. approximate each of these paths into sequences of straight lines or cubic Bézier curves using an adaptation of [the "potrace" software by Peter Selinger](http://potrace.sourceforge.net/), to possibly nonclosed paths with fixed end points.

The final components contours can then be reconstructed from the sequence of
simplified paths. Keeping the "triple points" fixed ensures that neighboring
components agree on their border.

Parallel implementation with OpenMP.  
Extension module for extracting shapefile format polygons with Python.  

### Directory tree
    .   
    ├── include/      C++ headers, with some doc  
    │   └── potrace/  modified C headers of potrace  
    ├── octave/       GNU Octave or Matlab code  
    │   ├── doc/      some documentation (to be written)  
    │   └── mex/      MEX C++ interfaces (only display for now)  
    ├── python/       Python code  
    │   └── cpython/  C Python interface  
    └── src/          C++ sources  
        └── potrace/  modified C sources of potrace  

### C++ documentation
Requires `C++11`.  
Be sure to have OpenMP enabled with your compiler to enjoy parallelization. Note that, as of 2020, MSVC still does not support OpenMP 3.0 (published in 2008); consider switching to a decent compiler.  

The C++ classes are documented within the corresponding headers in `include/`.  
### Python extension module for extracting shapefile format polygons
Requires `numpy` package.  
See the script `setup.py` for compiling the module with `distutils`; on UNIX systems, it can be directly interpreted as `python setup.py build_ext`.  

Once compiled, see the documentation with the `help()` python utility.

### License
This software is under the GPLv3 license.
