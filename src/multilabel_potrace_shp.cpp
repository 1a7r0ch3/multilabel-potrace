/*=============================================================================
 * Hugo Raguet 2020
 *===========================================================================*/
#include <limits>
#include "multilabel_potrace_shp.hpp"
#include "omp_num_threads.hpp"

#define TPL template <typename comp_t, typename int_coor_t>
#define MPT_SHP Multi_potrace_shp<comp_t, int_coor_t>
#define tMPT_SHP typename Multi_potrace_shp<comp_t, int_coor_t>

#define MALLOC(ptr, type, size) \
    ptr = (type*) malloc_check(sizeof(type)*((size_t) size));

using namespace std;

TPL MPT_SHP::Multi_potrace_shp(const comp_t* comp_assign, int_coor_t width,
        int_coor_t height, comp_t number_of_components, bool row_major)
    : Multi_potrace<comp_t, int_coor_t, shp_real_t>(comp_assign, width, height,
        number_of_components, row_major)
{
    polygons = nullptr;
    /* no smoothing: only straight lines in shapefile */
    set_smoothing(0.0, 0.0);
}

TPL MPT_SHP::~Multi_potrace_shp()
{ free_polygons(); }

TPL void MPT_SHP::free_polygons()
{
   if (polygons){
        for (comp_t comp = 0; comp < number_of_components; comp++){
            Shp_polygon& polygon = polygons[comp];
            free(polygon.parts);
            free(polygon.points);
        }
        free(polygons);
        polygons = nullptr;
    }
}

TPL tMPT_SHP::Shp_polygon MPT_SHP::compute_polygon(comp_t comp)
{
    Shp_polygon polygon;
    Shp_point& lower_left = polygon.bounding_box.lower_left;
    Shp_point& upper_right = polygon.bounding_box.upper_right;
    shp_int_t& number_of_parts = polygon.number_of_parts;
    shp_int_t& number_of_points = polygon.number_of_points;
    shp_int_t*& parts = polygon.parts;
    Shp_point*& points = polygon.points;

    const Contour_r& contour = contours[comp];

    number_of_parts = contour.number_of_borders;

    /* compute number of points */
    number_of_points = 0;
    for (int i = 0; i < contour.number_of_borders; i++){
        const Border_r& border = contour.borders[i];
        for (int j = 0; j < border.number_of_paths; j++){
            const Path_r& path = border.paths[j];
            number_of_points += path.length;
        }
        number_of_points += 1; // the first point of a ring/border is repeated
    }

    MALLOC(parts, shp_int_t, number_of_parts);
    MALLOC(points, Shp_point, number_of_points);

    lower_left.x = numeric_limits<shp_real_t>::max();
    lower_left.y = numeric_limits<shp_real_t>::max();
    upper_right.x = numeric_limits<shp_real_t>::lowest();
    upper_right.y = numeric_limits<shp_real_t>::lowest();

    int part_num = 0;
    int point_num = 0;
    for (int i = 0; i < contour.number_of_borders; i++){
        const Border_r& border = contour.borders[i];
        /* TODO: check that there is only one positive border, i.e. that the 
         * the component is actually connected; in particular, if two original
         * components are crossing each others through diagonaly connected
         * pixels, one of the two will get disconnected; a solution would be to
         * create an additional polygon, but this is currently not implemented;
         * NOTA: flagging several positive borders for the same component is
         * much easier when computing borders, see multilabel_potrace */
        int start_point_num = parts[part_num++] = point_num;
        /* the shapefile convention is to keep the interior of the polygon on
         * the right, which is the opposite of our convention */
        for (int j = border.number_of_paths - 1; j >= 0; j--){
            const Path_r& path = border.paths[j];
            /* if the list of points is taken from the other component, it is 
             * given in reverse order; but here "reverse" is correct order! */
            const bool is_reverse = comp > path.other_comp;
            for (int k = 0; k < path.length; k++){
                Shp_point& point = points[point_num++];
                point = is_reverse ? path.segments[k] :
                                     path.segments[path.length - k];
                if (point.x < lower_left.x){ lower_left.x = point.x; }
                if (point.x > upper_right.x){ upper_right.x = point.x; }
                if (point.y < lower_left.y){ lower_left.y = point.y; }
                if (point.y > upper_right.y){ upper_right.y = point.y; }
            }
        }
        /* the first point of a ring/border is repeated */
        points[point_num++] = points[start_point_num];
    }

    return polygon;
}

TPL void MPT_SHP::compute_polygons()
{
    trace();

    /* rough estimation of the total number of vertices to record;
     * useful for estimating the parallel work load */
    shp_int_t total_number_of_vertices = 0;
    for (comp_t comp = 0; comp < number_of_components; comp++){
        const Contour_r& contour = contours[comp];
        for (int i = 0; i < contour.number_of_borders; i++){
            const Border_r& border = contour.borders[i];
            for (int j = 0; j < border.number_of_paths; j++){
                const Path_r& path = border.paths[j];
                total_number_of_vertices += path.length;
            }
        }
    }
    int n_thrds = compute_num_threads(total_number_of_vertices,
        number_of_components);

    /* compute polygons in parallel */
    free_polygons();
    MALLOC(polygons, Shp_polygon, number_of_components);
    #pragma omp parallel for schedule(dynamic) num_threads(n_thrds)
    /* unsigned loop counter is allowed since OpenMP 3.0 (2008)
     * but MSVC compiler still does not support it as of 2020 */
    for (long comp = 0; comp < (long) number_of_components; comp++){
        polygons[comp] = compute_polygon(comp);
    }

    free_contours(contours);
}

TPL const tMPT_SHP::Shp_polygon& MPT_SHP::get_polygon(comp_t comp){
    if (!polygons){
        cerr << "Multilabel potrace shp: polygons not computed yet; "
            "make sure compute_polygons() is called before accessing them."
            << endl;
        exit(EXIT_FAILURE);
    }
    return polygons[comp];
}

/* instantiate for compilation */
template class Multi_potrace_shp<uint16_t, uint16_t>;
