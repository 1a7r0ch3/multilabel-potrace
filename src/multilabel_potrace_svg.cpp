/*=============================================================================
 * Hugo Raguet 2020
 *===========================================================================*/
#include "multilabel_potrace_svg.hpp"
#include <cmath> // floor and pow for truncation for precision

#define TPL template <typename comp_t>
#define MPT_SVG Multi_potrace_svg<comp_t>
#define tMPT typename Multi_potrace<comp_t, svg_int_t, svg_real_t>

using namespace std;

TPL MPT_SVG::Multi_potrace_svg(const comp_t* comp_assign, svg_int_t width,
        svg_int_t height, comp_t number_of_components, bool row_major,
        int precision)
    : Multi_potrace<comp_t, svg_int_t, svg_real_t>(comp_assign, width, height,
        number_of_components, row_major)
{
    comp_colors = nullptr;
    line_color = nullptr;
    line_width = 1;
    ten_to_prec = pow(10.0, precision);
}

TPL void MPT_SVG::set_precision(int precision)
{
    if (precision < 0){ precision = 0; }
    ten_to_prec = pow(10.0, precision);
}

TPL void MPT_SVG::set_style(int line_width, const svg_color_t* line_color,
        const svg_color_t* comp_colors)
{
    if (line_width < 0){ line_width = 0; }
    if (!line_width && !comp_colors){
        cerr << "Multilabel potrace SVG: zero line width and no fill color "
            " would render an empty page; check the requested style." << endl;
        exit(EXIT_FAILURE);
    }
    this->line_width = line_width;
    this->line_color = line_color;
    this->comp_colors = comp_colors;
}

/**  convert our data structures to SVG specifications  **/

TPL svg_real_t MPT_SVG::truncate(svg_real_t x) const
{ return floor(x*ten_to_prec + (svg_real_t) 0.5) / ten_to_prec; }

TPL template <typename coor_t>
void MPT_SVG::insert_point(const Point2D<coor_t>& point)
/* in SVG specifications, y-axis grows top to bottom */
{ svg_file << point.x << " " << height - point.y; }

/* overload for truncating real coordinate */
TPL void MPT_SVG::insert_point(const Point2D<svg_real_t>& point)
{ svg_file << truncate(point.x) << " " << truncate(height - point.y); }

TPL void MPT_SVG::insert_color(const svg_color_t* color)
{
    svg_file << "\"rgb(" << +color[0] << "," << +color[1] << "," << +color[2]
        << ")\"";
}

TPL template <typename Segment>
void MPT_SVG::insert_path_segment(const Segment& point)
{
    if (is_correct_order && !is_first_segment){ svg_file << "L"; }
    insert_point(point);
    if (!is_correct_order){ svg_file << "L"; }
    is_first_segment = false;
}

TPL void MPT_SVG::insert_path_segment(const Curve& curve)
{
    if (curve.is_vertex()){
        if (is_correct_order){ insert_path_segment(curve.control_points[1]); }
        if (curve.control_points[2] != curve.not_a_point()){
            insert_path_segment(curve.control_points[2]);
        }
        if (!is_correct_order){ insert_path_segment(curve.control_points[1]); }
    }else{
        if (is_correct_order){
            if (is_first_segment){
                /* opening with Bézier curve (the border is one cycle path) */
                insert_path_segment(curve.control_points[2]);
            }else{
                svg_file << "C"; insert_point(curve.control_points[0]);
                svg_file << ","; insert_point(curve.control_points[1]);
                svg_file << ","; insert_point(curve.control_points[2]);
            }
        }else{
            insert_point(curve.control_points[2]); svg_file << "C"; 
            insert_point(curve.control_points[1]); svg_file << ","; 
            insert_point(curve.control_points[0]); svg_file << ","; 
            is_first_segment = false;
        }
    }
}

TPL template <typename Segment>
void MPT_SVG::close_border(const Segment& last, const Segment& end)
{
            if (!is_correct_order){ // point - point - reverse
                svg_file.seekp(-1, ios_base::cur); // erase last "L"
            }
    svg_file << "z";
}

TPL void MPT_SVG::close_border(const Curve& last, const Curve& end)
{
    if (last.is_vertex()){
        if (end.is_vertex()){
            if (!is_correct_order){ // point - point - reverse
                svg_file.seekp(-1, ios_base::cur); // erase last "L"
            }
        }else{
            if (is_correct_order){ // point - curve - direct
                /* only last control point of end was inserted at first */
                insert_path_segment(end);
            }else{ // point - curve - reverse
                /* close the curve to the first control point of end */
                svg_file << "z"; 
            }
        }
    }else{
        if (!is_correct_order){ // curve - any - reverse
            /* last control points of the curve must be given; the end
             * vertex or curve necessarily specifies it */
            insert_point(end.control_points[2]);
        }else{
            if (!end.is_vertex()){ // curve - curve - direct
                /* only last control point of end was inserted at first */
                insert_path_segment(end);
            }
        }
    }

    if (end.is_vertex()){ svg_file << "z"; }
}

TPL template <typename Segment>
void MPT_SVG::draw_contour(const Contour<Segment>& contour, comp_t comp)
{
    svg_file << "<path d=\"";

    for (int i = 0; i < contour.number_of_borders; i++){
        const Border<Segment>& border = contour.borders[i];

        is_first_segment = true;
        for (int j = 0; j < border.number_of_paths; j++){
            /* the SVG convention is to leave the interior of the shape
             * on the right, which is the opposite of our convention */
            const Path<Segment>& path =
                border.paths[border.number_of_paths - 1 - j];

            bool was_correct_order = is_correct_order;
            /* if the list of point is taken from the other component, it is 
             * given in reverse order; but here "reverse" is correct order! */
            is_correct_order = comp > path.other_comp;

            if (is_first_segment){
                svg_file << "M";
            }else if (was_correct_order && !is_correct_order){
                svg_file << "L";
            }else if (!was_correct_order && is_correct_order){
                is_first_segment = true;
            }

            for (int k = 0; k < path.length; k++){
                insert_path_segment(is_correct_order ?
                    path.segments[k] : path.segments[path.length - k]);
            }
        }

        const Path<Segment>& path = border.paths[0];
        close_border(path.segments[is_correct_order ? path.length - 1 : 1],
                   path.segments[is_correct_order ? path.length : 0]);
    }
    svg_file << "\""; // close path definition

    if (line_width){
        if (line_color){ svg_file << " stroke="; insert_color(line_color); }
        else           { svg_file << " stroke=\"black\""; }
        if (line_width != 1){
            svg_file << " stroke-width=\"" << line_width << "\"";
        }
    }

    if (comp_colors){
        svg_file << " fill="; insert_color(comp_colors + 3*comp);
    }else{
        svg_file << " fill=\"none\"";
    }

    svg_file << "/>\n";
}


TPL template <typename Segment>
void MPT_SVG::write_svg(const char* filename, const Contour<Segment>* contours)
{
    svg_file.open(filename);

    /* ensure the default format; note that no reasonable image dimension
     * should reach 1e6 */
    svg_file.unsetf(ios::floatfield);
    svg_file.precision(6);
    
    svg_file << "<svg width=\"" << width << "\" height=\"" << height
        << "\" xmlns=\"http://www.w3.org/2000/svg\">\n";

    for (comp_t comp = 0; comp < number_of_components; comp++){
        draw_contour(contours[comp], comp);
    }

    svg_file << "</svg>\n";
    svg_file.close();
}

TPL void MPT_SVG::trace_and_export(const char* smoothed_filename,
        const char* simplified_filename, const char* raw_filename)
{
    trace(simplified_filename, raw_filename);

    if (smoothed_filename){
        write_svg(smoothed_filename, smoothed_contours);
    }
    if (simplified_filename){
        write_svg(simplified_filename, simplified_contours);
    }
    if (raw_filename){
        write_svg(raw_filename, raw_contours);
    }
}

/* instantiate for compilation */
template class Multi_potrace_svg<uint16_t>;
