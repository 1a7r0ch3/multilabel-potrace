/*=============================================================================
 * Hugo Raguet 2020
 *===========================================================================*/
#include <cmath> // for sqrt
#include "multilabel_potrace.hpp"
#include "omp_num_threads.hpp"
#include "potrace.hpp"

#define TPL template <typename comp_t, typename int_coor_t, \
    typename real_coor_t>
#define TPLS TPL template <typename Segment>
#define MPT Multi_potrace<comp_t, int_coor_t, real_coor_t>
#define tMPT typename Multi_potrace<comp_t, int_coor_t, real_coor_t>

#define REALLOC(ptr, type, size) \
    ptr = (type*) realloc_check(ptr, sizeof(type)*((size_t) size));
#define MALLOC(ptr, type, size) \
    ptr = (type*) malloc_check(sizeof(type)*((size_t) size));

using namespace std; 

TPL MPT::Multi_potrace(const comp_t* comp_assign, int_coor_t width,
    int_coor_t height, comp_t number_of_components, bool row_major)
    : comp_assign(comp_assign), row_major(row_major), width(width),
      height(height), number_of_components(number_of_components)
{
    raw_contours = nullptr;
    simplified_contours = nullptr;
    smoothed_contours = nullptr;
    bboxes = nullptr;
    border_statuses = nullptr;
    straight_line_tol = 0.5;
    smoothing = 1.0;
    curve_fusion_tol = 0.2;
}

TPL MPT::~Multi_potrace()
{
    free_contours(raw_contours);
    free_contours(simplified_contours);
    free_contours(smoothed_contours);
    free(border_statuses);
}

TPL void MPT::set_straight_line_tolerance(real_coor_t straight_line_tol){
    if (straight_line_tol < 0.0){ straight_line_tol = 0.0; }
    this->straight_line_tol = straight_line_tol;
}

TPL void MPT::set_smoothing(real_coor_t smoothing,
    real_coor_t curve_fusion_tol)
{
    if (smoothing < 0.0){ smoothing = 0.0; }
    if (smoothing >= 4/3.0){ smoothing = 4/3.0; }
    this->smoothing = smoothing;
    if (curve_fusion_tol < 0.0){ curve_fusion_tol = 0.0; }
    this->curve_fusion_tol = curve_fusion_tol;
}

/*-----------------------------------------------------------------------------
 * Step 1: decompose the borders of each components into paths which are either
 * closed, or connecting "triple points", which are points where three or more
 * components are joining
-----------------------------------------------------------------------------*/

/* convert (x, y) coordinates (origin at lower left corner of the raster)
 * into linear indexing depending on column- or row-major memory layout;
 * given our convention on point and pixel coordinates correspondance, the
 * first pixel of the raster is at coordinates (0, height - 1) */
TPL size_t MPT::coor_to_index(int_coor_t x, int_coor_t y) const
{
    return row_major ?
        x + (size_t) (height - (y + 1))*width :
        (size_t) (x + 1)*height - (y + 1);
}

TPL size_t MPT::ul(size_t index) const
{ return row_major ? index - 1 : index - height; }

TPL size_t MPT::ur(size_t index) const
{ return index; }

TPL size_t MPT::dl(size_t index) const
{ return row_major ? index + width - 1 : index - height + 1; }

TPL size_t MPT::dr(size_t index) const
{ return row_major ? index + width : index + 1; }

TPL void MPT::move_up(size_t& index) const
{ row_major ? index -= width : index -= 1; }

TPL void MPT::move_down(size_t& index) const
{ row_major ? index += width : index += 1; }

TPL void MPT::move_left(size_t& index) const
{ row_major ? index -= 1 : index -= height; }

TPL void MPT::move_right(size_t& index) const
{ row_major ? index += 1 : index += height; }

TPL tMPT::Direction MPT::turn_right(Direction d) const
{ return d == UP ? RIGHT : d == RIGHT ? DOWN : d == DOWN ? LEFT : UP; }

TPL tMPT::Direction MPT::turn_left(Direction d) const
{ return d == UP ? LEFT : d == LEFT ? DOWN : d == DOWN ? RIGHT : UP; }

TPL size_t MPT::get_border_pixel(size_t index, Direction d) const
{
    return d == UP   ?   ul(index) :
           d == DOWN ?   dr(index) :
           d == LEFT ?   dl(index) :
        /* d == RIGHT */ ur(index) ;
}

TPL bool MPT::is_border(size_t index, Direction d) const
{
    return border_statuses[get_border_pixel(index, d)] & 1 << d;
}

TPL void MPT::set_border(size_t index, Direction d)
{
    border_statuses[get_border_pixel(index, d)] |= 1 << d;
}

TPL void MPT::compute_bounding_boxes()
{
    MALLOC(bboxes, Bbox, number_of_components);
    for (comp_t comp = 0; comp < number_of_components; comp++){
        bboxes[comp].lower_left.x = numeric_limits<int_coor_t>::max();;
        bboxes[comp].lower_left.y = numeric_limits<int_coor_t>::max();;
        bboxes[comp].upper_right.x = numeric_limits<int_coor_t>::min();;
        bboxes[comp].upper_right.y = numeric_limits<int_coor_t>::min();;
    }
    for (int_coor_t x = 0; x < width; x++){
        size_t index = coor_to_index(x, 0); 
        for (int_coor_t y = 0; y < height; y++){
            comp_t comp = comp_assign[index];
            if (x < bboxes[comp].lower_left.x){
                bboxes[comp].lower_left.x = x;
            }
            if (y < bboxes[comp].lower_left.y){
                bboxes[comp].lower_left.y = y;
            }
            if (x + 1 > bboxes[comp].upper_right.x){
                bboxes[comp].upper_right.x = x + 1;
            }
            if (y + 1 > bboxes[comp].upper_right.y){
                bboxes[comp].upper_right.y = y + 1;
            }
            move_up(index); // y is incremented
        }
    }
}

TPL tMPT::Direction MPT::find_next_border(comp_t comp, Point_i& p) const
{
    const Bbox& bbox = bboxes[comp];

    for (; p.y < bbox.upper_right.y; p.y++){
        size_t index = coor_to_index(p.x, p.y);
        for (; p.x < bbox.upper_right.x; p.x++){
            if (comp_assign[ur(index)] == comp){
                if ((p.y == 0 || comp_assign[dr(index)] != comp) &&
                    !is_border(index, RIGHT)){
                    return RIGHT;
                }
            }else if (p.x > 0 && p.y > 0){
                if (comp_assign[ul(index)] == comp && !is_border(index, UP)){
                    return UP;
                }
            }
            move_right(index); // x is incremented
        }
        p.x = bbox.lower_left.x;
    }
    return DOWN;
}


/* useful for treating diagonaly connected components consistently, see
 * implementation of compute_raw_path() */
#define LOCAL_RADIUS 3
TPL comp_t MPT::smallest_comp(const Point_i& p, comp_t comp, comp_t other)
    const
{
    /* ensure no weird configuration */
    int_coor_t radius = LOCAL_RADIUS;
    if (radius > width){ radius = width; }
    if (radius > height){ radius = height; }

    size_t n_comp = 0;
    size_t n_other = 0;
    double radius_square = radius*radius;
    
    /* a. local size: count pixels witin a radius of LOCAL_RADIUS pixel */
    int_coor_t x_min = p.x > radius ? p.x - radius : 0;
    int_coor_t x_max = p.x < width - radius ? p.x + radius : width - 1;
    int_coor_t y_min = p.y > radius ? p.y - radius : 0;
    int_coor_t y_max = p.y < width - radius ? p.y + radius : width - 1;
    n_comp = 0;
    n_other = 0;
    for (int_coor_t x = x_min; x < x_max; x++){
        size_t index = coor_to_index(x, y_min); 
        for (int_coor_t y = y_min; y < y_max; y++){
            /* compute distance between point and pixel's center */
            double x_d = x + 0.5 - p.x;
            double y_d = y + 0.5 - p.y;
            if (x_d*x_d + y_d*y_d < radius_square){
                if (comp_assign[index] == comp){ n_comp++; }
                else if (comp_assign[index] == other){ n_other++; }
            }
            move_up(index); // y is incremented
        }
    }
    if (n_comp < n_other){ return comp; }
    else if (n_other < n_comp){ return other; }

    /* b. global size: check with bounding boxes */
    n_comp = (bboxes[comp].upper_right.x - bboxes[comp].lower_left.x)*
            (size_t)(bboxes[comp].upper_right.y - bboxes[comp].lower_left.y);
    n_other = (bboxes[other].upper_right.x - bboxes[other].lower_left.x)*
            (size_t)(bboxes[other].upper_right.y - bboxes[other].lower_left.y);
    if (n_comp < n_other){ return comp; }
    else if (n_other < n_comp){ return other; }
     
    /* c. decide arbitrary but consistently from components identifiers */
    if (comp < other){ return comp; }
    else{ return other; }
}

TPL bool MPT::compute_raw_path(Point_i& p, Direction& d, const Point_i& p0,
    Path<Point_i>& path)
{
    size_t index = coor_to_index(p.x, p.y);

    /* determine the components on both sides of the path */
    comp_t comp;
    if (d == UP){
        comp = comp_assign[ul(index)];
        path.other_comp = p.x == width ? out_comp() : comp_assign[ur(index)];
    }else if (d == DOWN){
        comp = comp_assign[dr(index)];
        path.other_comp = p.x == 0 ? out_comp() : comp_assign[dl(index)];
    }else if (d == LEFT){
        comp = comp_assign[dl(index)];
        path.other_comp = p.y == height ? out_comp() : comp_assign[ul(index)];
    }else{ // d == RIGHT
        comp = comp_assign[ur(index)];
        path.other_comp = p.y == 0 ? out_comp() : comp_assign[dr(index)];
    }

    int number_of_points = 0;
    int bufsize = comp < path.other_comp ? 16 : 2;
    MALLOC(path.segments, Point_i, bufsize);

    /* record the first point of the path for later identification */
    if (comp > path.other_comp){ path.first_point() = p;  }

    bool is_triple_point = false;
    bool is_first_point = true;

    while (true){ /* construct path */

        /* add point to path */
        if (comp < path.other_comp){ // only comp with lowest id records path
            if (number_of_points == bufsize) {
                bufsize += bufsize/2 + 1;
                REALLOC(path.segments, Point_i, bufsize);
            }
            path.segments[number_of_points++] = p;
        }
 
        /* check if triple point reached or if border is closed */
        if (is_triple_point || (p == p0 && !is_first_point)) { break; }

        /* flag border */
        set_border(index, d);

        /* move to next point and check surrounding pixels */
        comp_t on_left, on_right;
        if (d == UP){ // x cannot be 0
            p.y++;
            move_up(index);
            if (p.y == height){
                on_left = on_right = out_comp();
            }else{
                on_left = comp_assign[ul(index)];
                on_right = p.x == width ? out_comp() : comp_assign[ur(index)];
            }
        }else if (d == DOWN){ // x cannot be width
            p.y--;
            move_down(index);
            if (p.y == 0){
                on_left = on_right = out_comp();
            }else{
                on_left = comp_assign[dr(index)];
                on_right = p.x == 0 ? out_comp() : comp_assign[dl(index)];
            }
        }else if (d == LEFT){ // y cannot be 0
            p.x--;
            move_left(index);
            if (p.x == 0){
                on_left = on_right = out_comp();
            }else{
                on_left = comp_assign[dl(index)];
                on_right = p.y == height ? out_comp() : comp_assign[ul(index)];
            }
        }else{ // d == RIGHT // y cannot be height
            p.x++;
            move_right(index);
            if (p.x == width){
                on_left = on_right = out_comp();
            }else{
                on_left = comp_assign[ur(index)];
                on_right = p.y == 0 ? out_comp() : comp_assign[dr(index)];
            }
        }

        if (is_first_point){
            /* record the second point of the path for later identification */
            if (comp > path.other_comp){ path.second_point() = p;  }
            is_first_point = false;
        }

        /* determine next direction and check for triple point */

/* for diagonal connection to be processed consistently, one must ensure that
 * the path of the two components involved turn to opposite directions (since
 * they visit the point of connection coming from opposite directions);
 * but left or right is not completely arbitrary: given our convention to
 * follow the borders by keeping the component on the left, turning right would
 * connect the pieces connected by diagonal into the same object, whereas
 * turning left would separate them;
 * we use the following heuristics:
 *  1. if the components in the other diagonal are different, we consider the
 *     current component as a continuous object which separates two other
 *     objects; we thus turn right for connexity, and do not regard this point
 *     as a triple point (care must be taken when encountering it when
 *     computing the path for the other involved components)
 *  2. if the pixels of the other diagonal belongs to the same component, we
 *  consider the smallest object to be in front of the biggest: turn right for
 *  the former and left for the latter; smallest is determined by the first of
 *  the following criteria that discriminates the components:
 *      a. local size, by counting pixels around the point (within a radius of
 *      LOCAL_RADIUS pixel sides)
 *      b. if same local size, use global size, roughly determined by bounding
 *      boxes
 *      c. if still equal, choose arbitrarily the component with lowest
 *      component identifier as the smallest
 *
 * WARNING: in case 2., the biggest component might end up being disconnected;
 * moreover, the point of diagonal connection is not processed as a triple
 * point, and thus can move freely when tracing each side: this might, in
 * theory, lead to borders crossing each others; this is rarely happening in
 * practice; TODO: can we ensure that this does not happen at all? */

        if (on_left == comp){
            if (on_right == comp){ /* simple corner */
                d = turn_right(d);
            }else if (on_right != path.other_comp){
                is_triple_point = true;
            }/* else continue straight ahead */
        }else if (on_left == path.other_comp){
            if (on_right == comp){ /* connected by diagonal, case 2. */
                d = comp == smallest_comp(p, comp, path.other_comp) ?
                    turn_right(d) : turn_left(d);
            }else{ /* either corner, or other comp diagonal, case 1. */
                d = turn_left(d);
            }
        }else if (on_right == comp){ /* connected by diagonal, case 1. */
            d = turn_right(d);
        }else{ /* corner and triple point */
            d = turn_left(d);
            is_triple_point = true;
        }

    } /* path is complete */

    if (comp < path.other_comp){
        if (bufsize > number_of_points){
            REALLOC(path.segments, Point_i, number_of_points);
        }
        path.length = number_of_points - 1;
    }else{
        path.length = 0; /* flag path recorded by other component */
    }

    return is_triple_point;
}

TPL tMPT::template Border<tMPT::Point_i>
MPT::compute_raw_border(Point_i p, Direction d)
{
    Border<Point_i> border;
    int& number_of_paths = border.number_of_paths;
    Path<Point_i>*& paths = border.paths;

    number_of_paths = 0;
    int bufsize = 16;
    MALLOC(paths, Path<Point_i>, bufsize);

    Point_i p0 = p;

    bool is_triple_point;

    do {
        if (number_of_paths == bufsize){
            bufsize += bufsize/2 + 1;
            REALLOC(paths, Path<Point_i>, bufsize);
        }
        is_triple_point = compute_raw_path(p, d, p0, paths[number_of_paths++]);
    } while (p != p0);

    /* if the border contains more than one path and the origin was not a
     * triple point, the last path is in fact the begining of the first path */
    if (number_of_paths > 1 && !is_triple_point){
        number_of_paths--;
        Path<Point_i>& first_path = paths[0];
        Path<Point_i>& last_path = paths[number_of_paths];
        if (first_path.length){ /* paths recorded; append first to last */
            REALLOC(last_path.segments, Point_i,
                last_path.length + first_path.length + 1);
            for (int i = 1; i <= first_path.length; i++){
                last_path.segments[last_path.length + i] =
                    first_path.segments[i];
            }
            first_path.length += last_path.length;
        } /* else keep only two first points of the last in the first */
        free(first_path.segments);
        first_path.segments = last_path.segments;
    }

    if (bufsize > number_of_paths){
        REALLOC(paths, Path<Point_i>, number_of_paths);
    }
 
    return border;
}

TPL tMPT::template Contour<tMPT::Point_i>
MPT::compute_raw_contour(comp_t comp)
{
    Contour<Point_i> contour;
    int& number_of_borders = contour.number_of_borders;
    Border<Point_i>*& borders = contour.borders;

    number_of_borders = 0;
    int bufsize = 16;
    MALLOC(borders, Border<Point_i>, bufsize);

    /* start at lower left corner of bounding box */
    Point_i p = bboxes[comp].lower_left;
    Direction d = find_next_border(comp, p);

    while (d != DOWN){
        if (number_of_borders == bufsize){
            bufsize += bufsize/2 + 1;
            REALLOC(borders, Border<Point_i>, bufsize);
        }
        borders[number_of_borders++] = compute_raw_border(p, d);
        d = find_next_border(comp, p);
    }

    if (bufsize > number_of_borders){
        REALLOC(borders, Border<Point_i>, number_of_borders);
        /* safety: components identifiers in comp_assign may be nonsequential;
         * in that case, borders of nonexisting components must be set to null
         * we do it explicitely for dumb compilers such as MSVC */
        if (!number_of_borders){ borders = nullptr; }
    }

    return contour;
}

TPL void MPT::compute_raw_contours()
{
    compute_bounding_boxes();

    /* rough estimation of the total length of paths
     * useful for estimating the parallel work load */
    double avg_area = (double) height*width/number_of_components;
    /* area ~= ratio*radius^2 and perim ~= 2*ratio*radius, where ratio is pi
     * for a disk, 4 for a square (considering radius is half side length);
     * we use ratio = 3.5 in perim ~= 2*ratio*sqrt(area/ratio) */
    double avg_perimeter = 7.*sqrt(avg_area/3.5); 
    double total_length = avg_perimeter*number_of_components/2;
    int n_thrds = compute_num_threads(total_length, number_of_components);

    /* boolean map indicating the pixels which have been visited */
    MALLOC(border_statuses, Border_status, height*width);
    for (size_t index = 0; index < (size_t) height*width; index++){
        border_statuses[index] = 0;
    }

    free_contours(raw_contours);
    MALLOC(raw_contours, Contour<Point_i>, number_of_components);

    /* compute contours in parallel */
    #pragma omp parallel for schedule(dynamic) num_threads(n_thrds)
    /* unsigned loop counter is allowed since OpenMP 3.0 (2008)
     * but MSVC compiler still does not support it as of 2020 */
    for (long comp = 0; comp < (long) number_of_components; comp++){
        raw_contours[comp] = compute_raw_contour(comp);
    }

    free(border_statuses); border_statuses = nullptr;
    free(bboxes); bboxes = nullptr;
}

TPL void MPT::get_paths_from_lower_comp()
{
    for (comp_t comp = 0; comp < number_of_components; comp++){

        /* iterate over each path and find components with highest id */
        const Contour<Point_i>& contour = raw_contours[comp];
        for (int i = 0; i < contour.number_of_borders; i++){
            const Border<Point_i>& border = contour.borders[i];
            for (int j = 0; j < border.number_of_paths; j++){
                Path<Point_i>& path = border.paths[j];
                if (comp < path.other_comp){ continue; }

        /* find the correct path in the other component */
        const Contour<Point_i>& other_contour = raw_contours[path.other_comp];
        for (int k = 0; k < other_contour.number_of_borders; k++){
            const Border<Point_i>& other_border = other_contour.borders[k];
            for (int l = 0; l < other_border.number_of_paths; l++){
                const Path<Point_i>& other_path = other_border.paths[l];
                if (other_path.other_comp != comp){ continue; }

        /* path uniquely characterized by first and second points */
        if (path.first_point() == other_path.last_point() &&
            path.second_point() == other_path.second_to_last_point())
        {
            path.length = other_path.length;
            free(path.segments);
            path.segments = other_path.segments;
            if (simplified_contours){
                Path<Point_r>& simplified_path =
                    simplified_contours[comp].borders[i].paths[j];
                const Path<Point_r>& other_simplified_path =
                    simplified_contours[path.other_comp].borders[k].paths[l];
                simplified_path.length = other_simplified_path.length;
                simplified_path.segments = other_simplified_path.segments;
                simplified_path.other_comp = path.other_comp;
            }
            if (smoothed_contours){
                Path<Curve>& smoothed_path =
                    smoothed_contours[comp].borders[i].paths[j];
                const Path<Curve>& other_smoothed_path =
                    smoothed_contours[path.other_comp].borders[k].paths[l];
                smoothed_path.length = other_smoothed_path.length;
                smoothed_path.segments = other_smoothed_path.segments;
                smoothed_path.other_comp = path.other_comp;
            }
            goto path_found; // break two loops
        }

            } // end for other paths
        } // end for other borders

        /* control flow reaches this point if corresponding path not found */
        cerr << "Multilabel potrace: could not find corresponding path from "
            "component with lower identifier." << endl;
        exit(EXIT_FAILURE);

            path_found: ;

            } // end for paths
        } // end for borders

    } // end for components
}

/*-----------------------------------------------------------------------------
 * Step 2: approximate each of these paths into sequences of straight lines or
 * simple third order Bézier curves using an adaptation of "potrace" software
 * by Peter Selinger to possibly nonclosed paths with fixed end points.
 *---------------------------------------------------------------------------*/

/* assume raw_contours have been computed */
TPL void MPT::trace_path(comp_t comp, int border_num, int path_num)
{
    /* get borders and paths */
    const Path<Point_i>& raw_path =
        raw_contours[comp].borders[border_num].paths[path_num];

    /* process only from the component with lowest identifier */
    if (comp > raw_path.other_comp){
        if (simplified_contours){
            simplified_contours[comp].borders[border_num].paths[path_num]
                .segments = nullptr;
        }
        if (smoothed_contours){
            smoothed_contours[comp].borders[border_num].paths[path_num]
                .segments = nullptr;
        }
        return;
    }

    /* convert to the potrace path structure */
    potrace::privpath_t* potrace_path = potrace::privpath_new();
    bool is_cycle = raw_path.first_point() == raw_path.last_point();
    potrace_path->len = is_cycle ? raw_path.length : raw_path.length + 1;
    MALLOC(potrace_path->pt, potrace::point_t, potrace_path->len);
    for (int i = 0; i < potrace_path->len; i++){
        potrace_path->pt[i].x = raw_path.segments[i].x;
        potrace_path->pt[i].y = raw_path.segments[i].y;
    }

    /* trace the path with potrace */
    process_path(potrace_path, smoothing, curve_fusion_tol, straight_line_tol,
        is_cycle);

    /* copy result into multilabel potrace data structure */
    if (simplified_contours){
        const potrace::privcurve_t* potrace_curve = &potrace_path->curve;
        Path<Point_r>& simplified_path =
            simplified_contours[comp].borders[border_num].paths[path_num];
        simplified_path.length = potrace_curve->n - (is_cycle ? 0 : 1);
        MALLOC(simplified_path.segments, Point_r, simplified_path.length + 1);
        potrace::dpoint_t* v = potrace_curve->vertex;
        for (int i = 0; i < simplified_path.length; i++){
            simplified_path.segments[i] = Point_r(v[i].x, v[i].y);
        }
        simplified_path.segments[simplified_path.length] = is_cycle ?
            simplified_path.first_point() : Point_r(raw_path.last_point());
        simplified_path.other_comp = raw_path.other_comp;
    }

    if (smoothed_contours){
        const potrace::privcurve_t* potrace_curve = potrace_path->fcurve;
        Path<Curve>& smoothed_path =
            smoothed_contours[comp].borders[border_num].paths[path_num];
        smoothed_path.length = potrace_curve->n - (is_cycle ? 0 : 1);
        MALLOC(smoothed_path.segments, Curve, smoothed_path.length + 1);
        potrace::dpoint_t (*c)[3] =  potrace_curve->c;
        for (int i = 0; i < smoothed_path.length; i++){
            if (potrace_curve->tag[i] == POTRACE_CORNER){ 
                /* only one vertex; if next segment is a Bézier curve,
                 * add its first control point */
                int j = i + 1 < potrace_curve->n ? i + 1 : 0;
                smoothed_path.segments[i] =
                    potrace_curve->tag[j] == POTRACE_CORNER ?
                    Curve(Point_r(c[i][1].x, c[i][1].y)) :
                    Curve(Point_r(c[i][1].x, c[i][1].y),
                          Point_r(c[i][2].x, c[i][2].y));
            }else{
                smoothed_path.segments[i] = 
                    Curve(Point_r(c[i][0].x, c[i][0].y),
                          Point_r(c[i][1].x, c[i][1].y),
                          Point_r(c[i][2].x, c[i][2].y));
            }
        }
        smoothed_path.segments[smoothed_path.length] = is_cycle ?
            smoothed_path.first_point() : Curve(raw_path.last_point());
        smoothed_path.other_comp = raw_path.other_comp;
    }

    /* free potrace path structure */
    potrace::privpath_free(potrace_path);
}

TPL void MPT::trace(bool keep_simplified_stage, bool keep_raw_stage)
{
    compute_raw_contours();

    if (!straight_line_tol){
        get_paths_from_lower_comp();
        return;
    }
    
    /* get the total length of paths for estimating the parallel work load
     * based on a quadratic cost of tracing algorithm */
    long int total_square_length = 0;
    for (comp_t comp = 0; comp < number_of_components; comp++){
        const Contour<Point_i>& contour = raw_contours[comp];
        for (int i = 0; i < contour.number_of_borders; i++){
            const Border<Point_i>& border = contour.borders[i];
            for (int j = 0; j < border.number_of_paths; j++){
                const Path<Point_i>& path = border.paths[j];
                if (comp < path.other_comp){
                    total_square_length += path.length*path.length;
                }
            }
        }
    }
    int n_thrds = compute_num_threads(total_square_length,
        number_of_components);

    /* allocate the contours array only for requested information */
    if (!smoothing || keep_simplified_stage){
        free_contours(simplified_contours);
        MALLOC(simplified_contours, Contour<Point_r>, number_of_components);
    }
    if (smoothing){
        free_contours(smoothed_contours);
        MALLOC(smoothed_contours, Contour<Curve>, number_of_components);
    }

    /* trace contours in parallel */
    #pragma omp parallel for schedule(dynamic) num_threads(n_thrds)
    /* unsigned loop counter is allowed since OpenMP 3.0 (2008)
     * but MSVC compiler still does not support it as of 2020 */
    for (long comp = 0; comp < (long) number_of_components; comp++){
        Contour<Point_i>& contour = raw_contours[comp];
        if (simplified_contours){
            simplified_contours[comp].number_of_borders =
                contour.number_of_borders;
            MALLOC(simplified_contours[comp].borders,
                Border<Point_r>, contour.number_of_borders);
        }
        if (smoothed_contours){
            smoothed_contours[comp].number_of_borders =
                contour.number_of_borders;
            MALLOC(smoothed_contours[comp].borders,
                Border<Curve>, contour.number_of_borders);
        }
        for (int i = 0; i < contour.number_of_borders; i++){
            Border<Point_i>& border = contour.borders[i];
            if (simplified_contours){
                simplified_contours[comp].borders[i].number_of_paths =
                    border.number_of_paths;
                MALLOC(simplified_contours[comp].borders[i].paths,
                    Path<Point_r>, border.number_of_paths);
            }
            if (smoothed_contours){
                smoothed_contours[comp].borders[i].number_of_paths =
                    border.number_of_paths;
                MALLOC(smoothed_contours[comp].borders[i].paths,
                    Path<Curve>, border.number_of_paths);
            }
            for (int j = 0; j < border.number_of_paths; j++){
                trace_path(comp, i, j);
            }
        }
    }

    get_paths_from_lower_comp();
    if (!keep_raw_stage){ free_contours(raw_contours); }
}

/**  instantiate for compilation  **/
#include <cstdint>
template class Multi_potrace<uint16_t, uint16_t, float>;
template class Multi_potrace<uint16_t, uint16_t, double>;
