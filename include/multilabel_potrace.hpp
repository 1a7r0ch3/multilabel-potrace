/*=============================================================================
 * Extract and simplifies contours delimiting homogeneous connected components
 * within a 2D grid structure (typically, pixels of an image).
 * 
 * The assignment of each pixel to its component must be given. Then, this
 * software allows to perform the following steps :
 *
 * 1. decompose the borders of each components into paths which are either
 * closed, or connecting "triple points", which are points where three or more
 * components are joining 
 * 2. approximate each of these paths into sequences of straight lines or
 * cubic Bézier curves using an adaptation of the "potrace" software by Peter
 * Selinger, to possibly nonclosed paths with fixed end points.
 *
 * The final components contours can then be reconstructed from the sequence of
 * simplified paths. Keeping the "triple points" fixed ensures that neighboring
 * components agree on their border.
 *
 * Hugo Raguet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *===========================================================================*/
#pragma once
#include <cstdlib> // for size_t, malloc, exit
#include <iostream>
#include <type_traits> // for conditional typedef
#include <limits>

/* comp_t is the integer type for components identifiers;
 * int_coor_t is the integer type for integer coordinates (pixel corners);
 * real_coor_t is the floating type for adjusted coordinates on the raster */
template <typename comp_t, typename int_coor_t, typename real_coor_t>
class Multi_potrace
{
    /***  data types  ***/
// protected:
public:

    /* paths are represented as aggregate of "path segments";
     * polygons, which are shapes with only straight lines, are optimaly
     * represented directly with vertex points;
     * smooth vectorized paths uses Bézier curves approximating corners;
     * each path comprises its starting and ending segments, which are the same
     * whenever the path is a cycle */

    template <typename coor_t>
    struct Point2D
    {
        coor_t x, y;

        Point2D() {}
        Point2D(coor_t x, coor_t y) : x(x), y(y) {}
        template <typename other_coor_t>
        Point2D(const Point2D<other_coor_t>& p) : x(p.x), y(p.y) {}

        bool operator == (const Point2D<coor_t>& p) const
            { return x == p.x && y == p.y; }
        bool operator != (const Point2D<coor_t>& p) const
            { return x != p.x || y != p.y; }

        template <typename other_coor_t>
        const Point2D<coor_t>& operator = (const Point2D<other_coor_t>& p)
            { x = p.x; y = p.y; return *this; }
    };

    typedef Point2D<int_coor_t> Point_i;
    typedef Point2D<real_coor_t> Point_r;

    struct Bbox { Point_i lower_left, upper_right; };

    /**  vectorized curves  **/

    /* in potrace, a vectorized path is a sequence of curves, which are either
     *  - cubic Bézier curves defined by four control points, or
     *  - corner angles defined by three points
     *
     * for two such consecutive curve segments, the last control point of the
     * first one is always the first control point of the second one, so that
     * one can discard the first control point to avoid duplicate information;
     *
     * note in particular that for cyclic path, the first control of the first
     * curve is recorded by the last control point of the last curve
     *
     * moreover, if two consecutive curves are corner angles, intermediate
     * control points actually lie on the segment joining one vertex to the
     * other, so they can also be discarded
     *
     * finally, end points of noncyclic paths are similar to corner angles,
     * except that they do not need to specify an angle; one control point is
     * then enough, except for a starting point of a path whenever it is
     * followed by a Bézier curve and thus must store also the first control
     * point of this curve
     *
     * altogether, a curve segment can be represented by a three points array;
     *  - Bézier curve use all three points, actually constituting the three
     *  last of the necessary four control points representing it;
     *  - end points of noncyclic paths and corner angles followed by a Bézier
     *  curve use only both the middle point to store the vertex and the last
     *  point to store the first control point of the following Bézier curve;
     *  - end points of noncyclic paths and corner angles not followed by a
     *  Bézier curve use only the middle point to store the vertex;
     * the points in the array which are not used are flagged by a special
     * value not_a_point(), allowing identification of the configuration */
    struct Curve
    {
        Point_r control_points[3];

        /* flag for ignoring control points  */
        static Point_r not_a_point()
        {
            /* coordinates are by design supposed to be positive; after vertex
             * adjustments and simplification, negative coordinates are not
             * completely impossible, but should be close to zero anyway */
            return Point2D<real_coor_t>
                (std::numeric_limits<real_coor_t>::lowest(),
                 std::numeric_limits<real_coor_t>::lowest());
        }

        /* identify single vertex */
        bool is_vertex() const
        { return control_points[0] == not_a_point(); }

        /* only one vertex, used only on triple points at the end of a path;
         * template allowing for integer coordinate inputs from raw path */
        template <typename coor_t>
        Curve(const Point2D<coor_t>& v)
        {
            control_points[0] = not_a_point();
            control_points[1] = v;
            control_points[2] = not_a_point(); 
        }

        /* straight line segment */
        Curve(const Point_r& v, const Point_r& b)
        {
            control_points[0] = not_a_point();
            control_points[1] = v;
            control_points[2] = b;
        }

        /* Bézier curve segment */
        Curve(const Point_r& u, const Point_r& v, const Point_r& b)
        {
            control_points[0] = u;
            control_points[1] = v;
            control_points[2] = b;
        }
    
        /* only used for end points, vertex given by the middle one */
        bool operator == (const Curve& c) const
        { return control_points[1] == c.control_points[1]; }
    };

    /* conditional on the type of points within a segment: raw paths use
     * Point_i since coordinates are integers, simplified paths use Point_r
     * since non-triple points can be adjusted with real coordinates; and of
     * course curve use also real coordinates; requires C++11 */
    template <typename Segment> using Point = typename std::conditional<
        std::is_same<Segment, Point_i>::value, Point_i, Point_r>::type;

    /**  contours  **/

    /* a path links two triple points (or is a closed path if it is a border
     * involving only two components), and define a frontier between two
     * components; each path being involved in the border of two different
     * components, to save space and processing, only the component with
     * lowest id records and process the path;
     *
     * for the component with highest id to be able to retrieve the path, it
     * keeps only the two first points of the path, which can be compared to
     * last two points of each paths of the component with lowest id; this
     * gives access to the array listing the segments of the path, but beware
     * that for the component with highest id the order of the segments is
     * reversed.
     *
     * these structures are templated because the path can be constituted of:
     *  - points with integer coordinates for raw paths, or
     *  - points with real coordinates for optimal polygons, or
     *  - curve segments for smooth paths */

    template <typename Segment>
    struct Path
    {
        /* one less that the number of segments when the path is stored,
         * zero otherwise */
        int length; 
        Segment* segments;
        comp_t other_comp; // the other component

        /* get references to first and second points of a path */
        const Segment& first_point() const { return segments[0]; }
        const Segment& second_point() const { return segments[1]; }
        Segment& first_point() { return segments[0]; }
        Segment& second_point() { return segments[1]; }
        /* only valid if the path is stored */
        const Segment& second_to_last_point() const
            { return segments[length - 1]; }
        const Segment& last_point() const { return segments[length]; }
    };

    /* a border is a set of paths defining a closed curve */
    template <typename Segment>
    struct Border
    {
        int number_of_paths;
        Path<Segment>* paths;
    };

    /* a contour is a set of borders delimiting a component */
    template <typename Segment> struct Contour
    {
        int number_of_borders;
        Border<Segment>* borders;
    };

private:
    enum Direction {UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3};

    /* given a component, borders are iteratively constructed so as to keep the
     * component on its left;
     * thus, if a border encloses the component, we say that it is positive,
     * because if such a border is circular, it is visited in the positive
     * (trigonometric) sense; alternatively, if a border defines a hole in the
     * component, we say that it is negative, because if such a border is
     * circular, it is visited in the negative (clockwise) sense;
     * note that if components are connected, each has exactly one positive
     * border and can have any number of negative borders */

    /* flag on a pixel if one of its four sides is involved in a border;
     * requires only four bits */
    typedef unsigned char Border_status;

    /* map indicating borders already taken into account;
     * status is indicated on pixels, enabling work in parallel along
     * components;
     * borders are uniquely identified by a point and a direction, and the
     * border status is stored on the pixel directly on the left, in coherence
     * with the above convention; */
    Border_status* border_statuses;

    /* check if border pixel has a given status */
    bool is_border(size_t index, Direction d) const;
    /* set given status to border pixel */
    void set_border(size_t index, Direction d);

    /***  other members  ***/

private:
    /* component assignment of each pixel; monodimensional array */
    const comp_t* comp_assign;
    /* memory layout :
     * - column-major : pixels are linearly indexed first top-to-bottom then
     * left-to-right (the default)
     * - row-major : pixels are linearly indexed first left-to-right then
     * top-to-bottom */
    const bool row_major;

    /* bounding box of each component */
    Bbox* bboxes;

protected:
    const int_coor_t width, height;

    const comp_t number_of_components;

    /**  parameters for simplification with potrace  **/

    /* fidelity to the raster : how far (in l-inf distance, pixel unit) from a
     * path can a straight line approximate it; higher value favor coarse
     * polygons, with less line segments; original potrace software is written
     * with straight_line_tol equal to one half; if set to zero, only raw
     * contours are computed and no simplification is performed */
    real_coor_t straight_line_tol;
    /* potrace alphamax parameter; rougly, from the middle of segments defining
     * a corner, it corresponds to the maximum ratio between the distance to
     * the control points approximating the corner, and the corner point;
     * this quantity is never greater than 4/3, default to unity; if set to
     * zero, no smoothing is performed (only raw contours and simplified
     * polygons are computed) */
    real_coor_t smoothing; 
    /* potrace opttolerance parameter; rougly, maximum area approximation
     * error between simplified polyline and a single curve approximating it;
     * default to .2; if set to zero, no fusion is performed after smoothing */
    real_coor_t curve_fusion_tol; 

public:
    /* array containing the raw contours of each component */
    Contour<Point_i>* raw_contours;

    /* array containing the simplified contours of each component */
    Contour<Point_r>* simplified_contours;

    /* array containing the smoothed contours of each component */
    Contour<Curve>* smoothed_contours;

    /***  methods  ***/
public:
    Multi_potrace(const comp_t* comp_assign, int_coor_t width,
        int_coor_t height, comp_t number_of_components,
        bool row_major = false);
    ~Multi_potrace();

    /* manipulate parameters for simplification with potrace */
    void set_straight_line_tolerance(real_coor_t straight_line_tol = 0.5);
    void set_smoothing(real_coor_t smoothing = 1.0,
        real_coor_t curve_fusion_tol = 0.2);

    /* process all: extract paths, trace and record them in contour arrays;
     * keep intermediate stages if requested */
    void trace(bool keep_simplified_stage, bool keep_raw_stage);

    /* shortcut for same treatment of intermediate stages */
    void trace(bool keep_intermediate_stage = false)
    { trace(keep_intermediate_stage, keep_intermediate_stage); }


private:
    /* identifier for outside component; no component can have this id */
    static comp_t out_comp() { return std::numeric_limits<comp_t>::max(); }

    /*-------------------------------------------------------------------------
     * Step 1: decompose the borders of each components into paths 
     *-----------------------------------------------------------------------*/

    /* convert (x, y) coordinates (origin at lower left corner of the raster)
     * into linear indexing depending on column- or row-major memory layout */
    size_t coor_to_index(int_coor_t x, int_coor_t y) const;

    /* By convention, the point of coordinates (x, y) is the lower left corner
     * of the pixel of coordinates (x, y); the following convert the linear
     * index of this pixel to the linear index of other surrounding pixels
     *
     * ul  |  ur (this is the pixel of coordinates (x, y))
     * --(x,y)---
     * dl  |  dr
     *
     * the following manipulates corresponding linear indexes */
    size_t ul(size_t index) const;
    size_t ur(size_t index) const; 
    size_t dl(size_t index) const; 
    size_t dr(size_t index) const; 
    void move_up(size_t& index) const; 
    void move_down(size_t& index) const; 
    void move_left(size_t& index) const; 
    void move_right(size_t& index) const; 

    /* given a point and a direction, get the index of the pixel to the left */
    size_t get_border_pixel(size_t index, Direction d) const;

    /* compute next direction depending on current direction */
    Direction turn_right(Direction current_dir) const;
    Direction turn_left(Direction current_dir) const;

    void compute_bounding_boxes();

    /* determine which of two components is considered smallest;
     * useful for treating diagonaly connected components consistently,
     * see implementation of compute_raw_path() */
    comp_t smallest_comp(const Point_i& p, comp_t comp, comp_t other_comp)
        const;

    /* find the next point which is at a lower left corner of a border of the
     * given component; the grid is explored left-to-right then bottom-to-top,
     * starting from the given point;
     * path are computed so as to keep the considered component on the left,
     * thus the upper right pixel from the point lies within the given
     * component if the border is positive (enclosing the component), and
     * outside if the border is negative (hole border);
     * return the next direction leaving the component on the left, this is
     * either RIGHT for a positive border, or UP for a negative border;
     * return DOWN if no next border is available */
    /* TODO: flag components with several positive borders, i.e. that are not
     * connected; this can happen when only diagonaly connected */
    Direction find_next_border(comp_t comp, Point_i& p) const;

    /* compute a path from the given point in the given direction, leaving the
     * current component to the left of the path, stopping at next triple point
     * or p0; boolean return value indicate if end point is a triple point */
    bool compute_raw_path(Point_i& p, Direction& d, const Point_i& p0,
        Path<Point_i>& path);

    /* compute a raw border, starting at given point with given direction */
    Border<Point_i> compute_raw_border(Point_i p, Direction d);

    /* compute the raw contour of a given component;
     * the paths on the contour of the given component are recorded only if
     * the component on the other side has highest identifier; otherwise, only
     * the first point on the path is recorded for later identification */
    Contour<Point_i> compute_raw_contour(comp_t comp);

    /* compute raw contours of all components, in parallel;
     * recall that a given component has only access to the paths separating it
     * from components with highest identifiers */
    void compute_raw_contours();

    /*-------------------------------------------------------------------------
     * Step 2: approximate each paths into sequences of lines or Bézier curves
     *-----------------------------------------------------------------------*/

    /* trace a path and record its simplified and smooth version */
    void trace_path(comp_t comp, int border_num, int path_num);

    /*-------------------------------------------------------------------------
     * Annex: provide access to paths for components with highest identifiers
     *-----------------------------------------------------------------------*/

    /* each path are recorded and processed only by the bordering component
     * with lowest identifier (see Contour definition); the following provide
     * access to paths for components with highest identifiers by using the
     * raw contours:
     * - raw contours are constructed in such a way that a given path is the
     * same for both bordering components but with inverted point order;
     * correspondance is thus caracterized by matching the first and second
     * points recorded by the highest identifier (which records only these)
     * against the last and second to last points recorded by the lowest
     * identifier (which records the entire path)
     * - paths for a given components can be identified by two indices: one for
     * the border and one for the path; these indices will be stored in the
     * place of the coordinates of the first point (see below) */
    void get_paths_from_lower_comp();

    /**  memory management  **/
private:
    template <typename Segment> void free_path(Path<Segment>& path,
        comp_t comp);
    template <typename Segment> void free_border(Border<Segment>& border,
        comp_t comp);
    template <typename Segment> void free_contour(Contour<Segment>& contour,
        comp_t comp);

protected:
    template <typename Segment> void free_contours
        (Contour<Segment>*& contours);

    /* allocate memory and fail with error message if not successful */
    static void* malloc_check(size_t size);

    /* simply free if size is zero */
    static void* realloc_check(void* ptr, size_t size);

    /* some friend functions for displaying or debugging
     * https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Making_New_Friends
     * explain why this must be defined within the class declaration */
    template<typename coor_t>
    friend std::ostream& operator << (std::ostream& out,
        const Point2D<coor_t>& point)
    { return out << "(" << point.x << ", " << point.y << ")"; }

    friend std::ostream& operator << (std::ostream& out,
        const Curve& curve)
    {
        if (curve.control_points[0] != curve.not_a_point()){
            out << curve.control_points[0] << " ~ ";
        }
        out << curve.control_points[1];
        if (curve.control_points[2] != curve.not_a_point()){
            out << " - " << curve.control_points[2];
        }
        return out;
    }

    template<typename Segment>
    friend std::ostream& operator << (std::ostream& out,
        const Path<Segment>& path)
    {
        out << "[" << path.other_comp << "] " << path.segments[0];
        for (int i = 1; i < path.length; i++){
            out << " -> " << path.segments[i];
        }
        if (path.length){
            out << " -> " << path.segments[path.length];
        }else{ /* path is recorded by the other component */
            out << path.segments[1] << " - - -> ";
        }
        return out;
    }

    template<typename Segment>
    friend std::ostream& operator << (std::ostream& out,
        const Border<Segment>& border)
    {
        for (int i = 0; i < border.number_of_paths; i++){
            out << border.paths[i] << "\n";
        }
        return out;
    }

    template<typename Segment>
    friend std::ostream& operator << (std::ostream& out,
        const Contour<Segment>& contour)
    {
        for (int i = 0; i < contour.number_of_borders; i++){
            out << contour.borders[i] << "\n";
        }
        return out;
    }


    friend std::ostream& operator << (std::ostream& out,
        const Multi_potrace<comp_t, int_coor_t, real_coor_t>& mp)
    {
        if (mp.raw_contours){
            out << "\nRaw:\n";
            for (comp_t comp = 0; comp < mp.number_of_components; comp++){
                out << "\n\tcomponent " << comp << "\n\n"
                    << mp.raw_contours[comp];
            }
        }

        if (mp.simplified_contours){
            out << "\nSimplified:\n";
            for (comp_t comp = 0; comp < mp.number_of_components; comp++){
                out << "\n\tcomponent " << comp << "\n\n"
                    << mp.simplified_contours[comp];
            }
        }

        if (mp.smoothed_contours){
            out << "\nSmoothed:\n";
            for (comp_t comp = 0; comp < mp.number_of_components; comp++){
                out << "\n\tcomponent " << comp << "\n\n"
                    << mp.smoothed_contours[comp];
            }
        }

        return out;
    }
};

/* some definitions must be available outside the class */

#define TPL template <typename comp_t, typename int_coor_t, \
    typename real_coor_t>
#define TPLS TPL template <typename Segment>
#define MPT Multi_potrace<comp_t, int_coor_t, real_coor_t>
#define tMPT typename Multi_potrace<comp_t, int_coor_t, real_coor_t>

TPLS void MPT::free_path(Path<Segment>& path, comp_t comp)
{
    if (comp < path.other_comp){
        free(path.segments);
        path.segments = nullptr; // prevent against double freeing
    }
}

TPLS void MPT::free_border(Border<Segment>& border, comp_t comp)
{
    for (int j = 0; j < border.number_of_paths; j++){
        free_path(border.paths[j], comp);
    }
    free(border.paths);
    /* prevent against double freeing */
    border.number_of_paths = 0;
    border.paths = nullptr;
}

TPLS void MPT::free_contour(Contour<Segment>& contour, comp_t comp)
{
    for (int i = 0; i < contour.number_of_borders; i++){
        free_border(contour.borders[i], comp);
    }
    free(contour.borders);
    /* prevent against double freeing */
    contour.number_of_borders = 0;
    contour.borders = nullptr;
}

TPLS void MPT::free_contours(Contour<Segment>*& contours)
{
    if (contours){
        for (comp_t comp = 0; comp < number_of_components; comp++){
            free_contour(contours[comp], comp);
        }
        free(contours);
        contours = nullptr;
    }
}

TPL void* MPT::malloc_check(size_t size){
    void *ptr = malloc(size);
    if (!ptr){
        std::cerr << "Multilabel potrace: not enough memory." << std::endl;
        exit(EXIT_FAILURE);
    }
    return ptr;
}

/* simply free if size is zero */
TPL void* MPT::realloc_check(void* ptr, size_t size){
    if (!size){
       free(ptr); 
       return nullptr; 
    }
    ptr = realloc(ptr, size);
    if (!ptr){
        std::cerr << "Multilabel potrace: not enough memory." << std::endl;
        exit(EXIT_FAILURE);
    }
    return ptr;
}

#undef TPL
#undef TPLS
#undef MPT
#undef tMPT
