/*=============================================================================
 * Derived class converting components contours into scalable vector graphics
 * file
 *
 * Hugo Raguet 2020
 *===========================================================================*/
#pragma once
#include "multilabel_potrace.hpp"
#include <fstream>

#include <cstdint>
/* not supposed to process huge images
 * see https://oreillymedia.github.io/Using_SVG/extras/ch08-precision.html */
typedef float svg_real_t;
typedef uint16_t svg_int_t;
typedef uint8_t svg_color_t;

/* comp_t is the integer type for components identifiers */
template <typename comp_t>
class Multi_potrace_svg : public Multi_potrace<comp_t, svg_int_t, svg_real_t>
{
    /***  members  ***/
private:
    /**  style parameters  **/
    /* 3-by-number of components array specifying colors (rgb)
     * leave null (default) for no coloring */
    const svg_color_t* comp_colors;
    /* length 3 array specifying color (rgb); leave null (default) for black */
    const svg_color_t* line_color;
    int line_width; // default is 1

    /**  ofstream object to write on a file  **/
    std::ofstream svg_file;

    /* internal state */ 
    bool is_first_segment;
    bool is_correct_order;

    /**  for decimal truncation according to precision  **/
    svg_real_t ten_to_prec;

    /***  methods  ***/
public:
    /* precision is the maximum number of decimal numbers in the coordinates;
     * note that order of magnitude will be the pixel, see
     * https://oreillymedia.github.io/Using_SVG/extras/ch08-precision.html
     * so a precision of 2 should be largely enough */
    Multi_potrace_svg(const comp_t* comp_assign, svg_int_t width,
        svg_int_t height, comp_t number_of_components, bool row_major = false,
        int precision = 2);

    void set_precision(int precision);

    void set_style(int line_width,
        const svg_color_t* line_color = nullptr,
        const svg_color_t* comp_colors = nullptr);

    /* overload for only fill colors with no lines */
    void set_style(const svg_color_t* comp_colors)
    { set_style(0, nullptr, comp_colors); }

    /* trace the components and write the result into SVG files using current
     * parameters; if requested, intermediate stages are also written */
    void trace_and_export(const char* smoothed_filename,
                          const char* simplified_filename,
                          const char* raw_filename);

    /* overload with only one intermediate stage */
    void trace_and_export(const char* filename,
                          const char* intermediate_filename = nullptr)
    {
        if (straight_line_tol && smoothing){
            trace_and_export(filename, intermediate_filename, nullptr);
        }else if (straight_line_tol){ // no smoothing
            trace_and_export(nullptr, filename, intermediate_filename);
        }else{ // no simplification
            trace_and_export(nullptr, nullptr, filename);
        }
    }

private:
    using typename Multi_potrace<comp_t, svg_int_t, svg_real_t>::Curve;

    /* alias template requires C++11 */
    template <typename coor_t> using Point2D = typename
        Multi_potrace<comp_t, svg_int_t, svg_real_t>:: template Point2D<coor_t>;
    template <typename Segment> using Contour = typename
        Multi_potrace<comp_t, svg_int_t, svg_real_t>:: template Contour<Segment>;
    template <typename Segment> using Border = typename
        Multi_potrace<comp_t, svg_int_t, svg_real_t>:: template Border<Segment>;
    template <typename Segment> using Path = typename
        Multi_potrace<comp_t, svg_int_t, svg_real_t>:: template Path<Segment>;

    /**  convert our data structures to SVG specifications  **/

    /* truncate decimal to desired precision */
    svg_real_t truncate(svg_real_t x) const;

    template<typename coor_t>
    void insert_point(const Point2D<coor_t>& point);
    /* overload for truncating real coordinate */
    void insert_point(const Point2D<svg_real_t>& point);

    void insert_curve(const Curve& curve);

    void insert_color(const svg_color_t* color);

    /* generic insertion and closure for vertex segments (single points) */
    template <typename Segment>
    void insert_path_segment(const Segment& point);
    template <typename Segment>
    void close_border(const Segment& last, const Segment& end);

    /* specializations for curve segments (potentially Bézier) */
    void insert_path_segment(const Curve& curve);
    void close_border(const Curve& last, const Curve& end);

    template <typename Segment>
    void draw_contour(const Contour<Segment>& contours, comp_t comp);

    template <typename Segment>
    void write_svg(const char* filename, const Contour<Segment>* contours);
    
    using Multi_potrace<comp_t, svg_int_t, svg_real_t>::number_of_components;
    using Multi_potrace<comp_t, svg_int_t, svg_real_t>::width;
    using Multi_potrace<comp_t, svg_int_t, svg_real_t>::height;
    using Multi_potrace<comp_t, svg_int_t, svg_real_t>::straight_line_tol;
    using Multi_potrace<comp_t, svg_int_t, svg_real_t>::smoothing;
    using Multi_potrace<comp_t, svg_int_t, svg_real_t>::smoothed_contours;
    using Multi_potrace<comp_t, svg_int_t, svg_real_t>::simplified_contours;
    using Multi_potrace<comp_t, svg_int_t, svg_real_t>::raw_contours;
    using Multi_potrace<comp_t, svg_int_t, svg_real_t>::trace;
};
