/* Copyright (C) 2001-2019 Peter Selinger.
   This file is part of Potrace. It is free software and it is covered
   by the GNU General Public License. See the file COPYING for details. */


#ifndef TRACE_H
#define TRACE_H

#include "potracelib.h"
#if 0
#include "progress.h"
#endif
#include "curve.h"

#if 0
int process_path(path_t *plist, const potrace_param_t *param, progress_t *progress);
#endif

int process_path(privpath_t *path, double alphamax, double opttolerance,
    double straight_line_tol, int is_cycle);

#endif /* TRACE_H */
