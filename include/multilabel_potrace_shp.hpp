/*=============================================================================
 * Derived class converting components contours into shapefile format polygons
 *
 * Hugo Raguet 2020
 *===========================================================================*/
#pragma once
#include "multilabel_potrace.hpp"

typedef double shp_real_t;
typedef int32_t shp_int_t;

/* comp_t is the integer type for components identifiers;
 * int_coor_t is the integer type for integer coordinates (pixel corners) */
template <typename comp_t, typename int_coor_t>
class Multi_potrace_shp : public Multi_potrace<comp_t, int_coor_t, shp_real_t>
{
    /***  data types  ***/
public:
    struct Shp_point {
        shp_real_t x, y;
        /* this assignment operator could be customized according to the
         * conversion between raster coordinates and geographic coordinates */
        const Shp_point& operator = (const typename
            Multi_potrace<comp_t, int_coor_t, shp_real_t>::Point_r& p)
            { x = p.x; y = p.y; return *this; }
    };

    struct Shp_bbox { Shp_point lower_left, upper_right; };

    struct Shp_polygon {
        /* bounding box of the polygon, in the order Xmin, Ymin, Xmax, Ymax */
        Shp_bbox bounding_box; 

        shp_int_t number_of_parts; // number of rings in the polygon

        shp_int_t number_of_points; // total number of points for all rings.

        /* array of length number_of_parts; stores, for each ring, the index of
         * its first point in the points array; array indexes are with respect
         * to 0 */
        shp_int_t* parts;

        /* array of length number_of_points; the points for each ring in the
         * polygon are stored end to end; the points for Ring 2 follow the
         * points for Ring 1, and so on; the parts array holds the array index
         * of the starting point for each ring; there is no delimiter in the
         * points array between rings */
        Shp_point* points;
    };

    /* NOTA: in our multilabel potrace naming convention, a polygon is defined
     * by a contour and a ring or part is called a border */

    /***  members  ***/
private:
    Shp_polygon* polygons;

    /***  methods  ***/
public:
    Multi_potrace_shp(const comp_t* comp_assign, int_coor_t width,
        int_coor_t height, comp_t number_of_components,
        bool row_major = false);
    ~Multi_potrace_shp();

    /* accessor to polygons; make sure compute_polygons() is called before */
    const Shp_polygon& get_polygon(comp_t comp);

private:
    void free_polygons();

private:
    /* local to a component */
    Shp_polygon compute_polygon(comp_t comp);

public:
    /* extract and decompose components contours, simplify and adjust with
     * potrace, and convert into shapefile format polygons;
     * work in parallel along components */
    /* TODO: deal with diagonaly connected components that gets disconnected
     * when tracing them; see multilabel_potrace */
    void compute_polygons(); 

private:
    using Multi_potrace<comp_t, int_coor_t, shp_real_t>::number_of_components;
    using Multi_potrace<comp_t, int_coor_t, shp_real_t>::simplified_contours;
    using Multi_potrace<comp_t, int_coor_t, shp_real_t>::set_smoothing;
    using Multi_potrace<comp_t, int_coor_t, shp_real_t>::malloc_check;
    using Multi_potrace<comp_t, int_coor_t, shp_real_t>::trace;
    using Multi_potrace<comp_t, int_coor_t, shp_real_t>::free_contours;

    using typename Multi_potrace<comp_t, int_coor_t, shp_real_t>::Point_r;

    /* specialize some types, variable names and methods */
    typedef typename Multi_potrace<comp_t, int_coor_t, shp_real_t>::
        template Path<Point_r> Path_r;
    typedef typename Multi_potrace<comp_t, int_coor_t, shp_real_t>::
        template Border<Point_r> Border_r;
    typedef typename Multi_potrace<comp_t, int_coor_t, shp_real_t>::
        template Contour<Point_r> Contour_r;

    Contour_r*& contours = simplified_contours;

    /* some friend functions for displaying or debugging
     * https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Making_New_Friends
     * explain why this must be defined within the class declaration */
    friend std::ostream& operator << (std::ostream& out,
        const Shp_point& point)
    { return out << "(" << point.x << ", " << point.y << ")"; }

    friend std::ostream& operator << (std::ostream& out,
        const Shp_polygon& polygon)
    {
        if (!polygon.number_of_points){ return out; }
        out << "[" << polygon.bounding_box.lower_left << " " 
            << polygon.bounding_box.upper_right << "] " << "\n";
        int i = 0;
        for (int j = 0; j < polygon.number_of_points; j++){
            if (polygon.parts[i] == j){ out << "\n <> "; i++; }
            out << polygon.points[j] << " " ;
        }
        return out << "\n";
    }

    friend std::ostream& operator << (std::ostream& out,
        const Multi_potrace_shp<comp_t, int_coor_t>& mp_shp)
    {
        if (!mp_shp.polygons){ return out; }
        for (comp_t comp = 0; comp < mp_shp.number_of_components; comp++){
            out << "\n\tcomponent " << comp << "\n\n" << mp_shp.polygons[comp];
        }
        return out;
    }
};
