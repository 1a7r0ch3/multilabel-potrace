origDir = pwd; % remember working directory
cd(fileparts(which('compile_multilabel_potrace_mex.m'))); 
mkdir('bin/');
try
    % compilation flags 
    [~, CXXFLAGS] = system('mkoctfile -p CXXFLAGS');
    [~, LDFLAGS] = system('mkoctfile -p LDFLAGS');
    % some versions introduces a newline character (10)
    % in the output of 'system'; this must be removed
    if CXXFLAGS(end)==10, CXXFLAGS = CXXFLAGS(1:end-1); end
    if LDFLAGS(end)==10, LDFLAGS = LDFLAGS(1:end-1); end
    CXXFLAGSorig = CXXFLAGS;
    LDFLAGSorig = LDFLAGS;
    % _GLIBCXX_PARALLEL is only useful for libstdc++ users
    % MIN_OPS_PER_THREAD roughly controls parallelization, see doc in README.md
    CXXFLAGS = [CXXFLAGS ' -Wextra -Wpedantic -std=c++11 -fopenmp -g0 ' ...
        '-DMIN_OPS_PER_THREAD=10000'];
    LDFLAGS = [LDFLAGS ',-fopenmp'];
    setenv('CXXFLAGS', CXXFLAGS);
    setenv('LDFLAGS', LDFLAGS);

    mex -I../include/potrace/ ../src/potrace/trace.c ../src/potrace/curve.c -c 

    mex -I../include/ mex/multilabel_potrace_mex.cpp ...
        ../src/multilabel_potrace.cpp curve.o trace.o ...
        -output bin/multilabel_potrace_mex
    clear multilabel_potrace_mex

    mex -I../include/ mex/multilabel_potrace_shp_mex.cpp ...
        ../src/multilabel_potrace_shp.cpp ../src/multilabel_potrace.cpp ...
        curve.o trace.o  -output bin/multilabel_potrace_shp_mex
    clear multilabel_potrace_shp_mex

    system('rm *.o');
catch % if an error occur, makes sure not to change the working directory
    % back to original environment
    setenv('CXXFLAGS', CXXFLAGSorig);
    setenv('LDFLAGS', LDFLAGSorig);
    cd(origDir);
	rethrow(lasterror);
end
% back to original environment
setenv('CXXFLAGS', CXXFLAGSorig);
setenv('LDFLAGS', LDFLAGSorig);
cd(origDir);
