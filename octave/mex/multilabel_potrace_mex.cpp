/*=============================================================================
 * [Points, Polygons] = multilabel_potrace_mex(comp_assign, [options])
 *
 * options is a struct with any of the following fields [with default values]:
 *      [nothing yet]
 *
 * TODO: output arrays (currently, only a command-line display of the
 * components contours is performed)
 *
 *  Hugo Raguet 2020
 *===========================================================================*/
#include <cstdint>
#include <cstring>
#include <iostream>
#include "mex.h"
#include "multilabel_potrace.hpp"

using namespace std;

/* comp_t must be able to represent the number of constant connected components
 * int_coor_t and real_coor_t must be able to represent input dimensions */
typedef uint16_t comp_t;
#define mxCOMP_CLASS mxUINT16_CLASS
#define COMP_CLASS_NAME "uint16"
typedef uint16_t int_coor_t;
#define mxINT_COOR_CLASS mxUINT16_CLASS
typedef float real_coor_t;
#define mxREAL_COOR_CLASS mxSINGLE_CLASS

/* function for checking optional parameters */
static void check_opts(const mxArray* options)
{
    if (!options){ return; }

    if (!mxIsStruct(options)){
        mexErrMsgIdAndTxt("MEX", "Multilabel Potrace: second parameter "
            "'options' should be a structure, (%s given).",
            mxGetClassName(options));
    }

    const int num_allow_opts = 0;
    const char* opts_names[] = {"dummy"};

    const int num_given_opts = mxGetNumberOfFields(options);

    for (int given_opt = 0; given_opt < num_given_opts; given_opt++){
        const char* opt_name = mxGetFieldNameByNumber(options, given_opt);
        int allow_opt;
        for (allow_opt = 0; allow_opt < num_allow_opts; allow_opt++){
            if (strcmp(opt_name, opts_names[allow_opt]) == 0){ break; }
        }
        if (allow_opt == num_allow_opts){
            mexErrMsgIdAndTxt("MEX", "Multilabel Potrace: option '%s' "
                "unknown.", opt_name);
        }
    }
}

/* function for checking parameter type */
static void check_arg_class(const mxArray* arg, const char* arg_name,
    mxClassID class_id, const char* class_name)
{
    if (mxGetNumberOfElements(arg) > 1 && mxGetClassID(arg) != class_id){
        mexErrMsgIdAndTxt("MEX", "Multilabel Portrace: parameter '%s' should "
            "be of class %s (%s given).", arg_name, class_name,
            mxGetClassName(arg), class_name);
    }
}

/* resize memory buffer allocated by mxMalloc and create a row vector */
template <typename type_t>
static mxArray* resize_and_create_mxRow(type_t* buffer, size_t size,
    mxClassID class_id)
{
    mxArray* row = mxCreateNumericMatrix(0, 0, class_id, mxREAL);
    if (size){
        mxSetM(row, 1);
        mxSetN(row, size);
        buffer = (type_t*) mxRealloc((void*) buffer, sizeof(type_t)*size);
        mxSetData(row, (void*) buffer);
    }else{
        mxFree((void*) buffer);
    }
    return row;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* get inputs */
    check_arg_class(prhs[0], "comp_assign", mxCOMP_CLASS, COMP_CLASS_NAME);
    int_coor_t height = mxGetM(prhs[0]);
    int_coor_t width = mxGetN(prhs[0]);
    const comp_t* comp_assign = (comp_t*) mxGetData(prhs[0]);

    /* get number of components */
    comp_t number_of_components = 0;
    for (size_t index = 0; index < height*width; index++){
        if (comp_assign[index] > number_of_components){
            number_of_components = comp_assign[index];
        }
    }
    number_of_components += 1; // components id start at zero
    
    #if 0
    /* check that all components are assigned */
    bool* is_assigned = (bool*) mxMalloc(sizeof(bool)*number_of_components);
    for (comp_t c = 0; c < number_of_components; c++){
        is_assigned[c] = false;
    }
    for (size_t index = 0; index < height*width; index++){
        is_assigned[comp_assign[index]] = true;
    }
    for (comp_t c = 0; c < number_of_components; c++){
        if (!is_assigned[c]){
            mexErrMsgIdAndTxt("MEX", "Multilabel Potrace: components "
            "identifiers go up to %s, but identifier %s never assigned.",
            number_of_components - 1, c);
        }
    }
    mxFree(is_assigned);
    #endif

    Multi_potrace<comp_t, int_coor_t, real_coor_t>* mp = 
        new Multi_potrace<comp_t, int_coor_t, real_coor_t>
            (comp_assign, width, height, number_of_components);

    mp->trace(true);

    cout << *mp << endl;

    delete mp;
}
