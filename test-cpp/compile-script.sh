#!/bin/zsh
# TODO: write a Makefile !

# first compile the modified potrace mechanisms
# gcc -I../include/potrace/ ../src/potrace/trace.c ../src/potrace/curve.c -c

# then compile the desired programs
g++ -I../include/ test_multilabel_potrace.cpp \
    ../src/multilabel_potrace.cpp curve.o trace.o \
    -o test_multi_pot -Wfatal-errors

g++ -I../include/ test_multilabel_potrace_svg.cpp \
    ../src/multilabel_potrace_svg.cpp \
    ../src/multilabel_potrace.cpp curve.o trace.o \
    -o test_multi_pot_svg -Wfatal-errors

