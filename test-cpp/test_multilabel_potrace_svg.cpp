#include "multilabel_potrace_svg.hpp"
#include <iostream>

#define WIDTH 100
#define HEIGHT 80

int main()
{
    bool row_major = false; // test with column-major layout
    uint16_t comp_assign[HEIGHT*WIDTH];
    for (int x = 0; x < WIDTH; x++){
    for (int y = 0; y < HEIGHT; y++){
        double a = (double) x/WIDTH; 
        double b = (double) y/HEIGHT; 
        /* index column-major */
        comp_assign[HEIGHT*x + (HEIGHT-1-y)] =
            /* small disks on top ... */
            (a - 0.6)*(a - 0.6) + (b - 0.6)*(b - 0.6) <= 0.1*0.1 ? 4 :
            (a - 0.25)*(a - 0.25) + (b - 0.25)*(b - 0.25) <= 0.1*0.1 ? 3 :
            /* ... of a bigger disk on top ... */
            (a - 0.5)*(a - 0.5) + (b - 0.5)*(b - 0.5) <= 0.4*0.4 ? 2 :
            /* ... of a triangle */
            a + b <= 0.5 ? 1 : 0;
    }}
    int number_of_components = 5;

    Multi_potrace_svg<uint16_t>* mp_svg = new Multi_potrace_svg<uint16_t>
        (comp_assign, WIDTH, HEIGHT, number_of_components, row_major);

    const svg_color_t line_color[3] = {100, 100, 100};
    const svg_color_t comp_colors[15] = {233, 233,  93,
                                         123,  34,  99,
                                           0, 255, 128,
                                          10,  20,  30,
                                         200, 100,   0};
    // mp_svg->set_style(2, line_color, comp_colors); 
    mp_svg->set_style(comp_colors); 

    mp_svg->set_straight_line_tolerance(0.5);
    mp_svg->set_smoothing(1.0, 0.2);
    mp_svg->trace_and_export("smooth.svg", "simple.svg", "raw.svg");

    std::cout.precision(2);
    std::cout << *mp_svg << std::endl;

    delete mp_svg;

    return 0;
}
