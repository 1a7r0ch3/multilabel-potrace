#include "multilabel_potrace_svg.hpp"

#include <iostream>

#define TEST_SVG_ROW_MAJOR 1

int main()
{
    bool row_major = true;
    #define HEIGHT 7
    /* #define WIDTH 15
    uint16_t comp_assign[] = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3,
        0, 0, 0, 0, 0, 1, 1, 0, 0, 3, 0, 3, 3, 3, 3,
        0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 3, 3, 3, 3,
        0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 3, 3, 3, 3, 2,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 3, 3, 3, 3, 2,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 3, 3, 2,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2
    };
    int number_of_components = 4; */
    #define WIDTH 9
    uint16_t comp_assign[] = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 1, 1, 0, 0, 
        0, 1, 0, 0, 0, 1, 1, 1, 0, 
        0, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 
        1, 1, 1, 1, 1, 1, 1, 1, 1, 
    };
    int number_of_components = 2;

    for (int y = HEIGHT - 1; y >= 0; y--){
        for (int x = 0; x < WIDTH; x++){
            /* index column-major */
            std::cout << comp_assign[x + (HEIGHT-1-y)*WIDTH];
        }
        std::cout << std::endl;
    }

    Multi_potrace<uint16_t, uint16_t, double>* mp =
        new Multi_potrace<uint16_t, uint16_t, double>
            (comp_assign, WIDTH, HEIGHT, number_of_components, row_major);

    mp->set_straight_line_tolerance(0.5);
    mp->set_smoothing(0.0, 0.0); // polygons only
    // mp_svg->set_smoothing(1.0, 0.0);

    mp->trace();

    std::cout.precision(2);
    std::cout << *mp << std::endl;

    delete mp;

    return 0;
}
