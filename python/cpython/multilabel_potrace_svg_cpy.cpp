/*=============================================================================
 * Python extension module for Multilabel Potrace SVG
 * 
 * Hugo Raguet 2021
 *===========================================================================*/
#include <cstdint>
/* #include <iostream> // for debugging */
#include <limits>
#include <cstdio>
#include <cstring>
#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#include "multilabel_potrace_svg.hpp"

typedef uint16_t comp_t; // we do not expect more than 65535 components
#define NPY_COMP NPY_UINT16
#define NPY_COMP_ALT NPY_INT16 // can be safely cast as NPY_COMP
#define COMP_T_STRING "uint16 or int16"

/* see multilabel_potrace_svg.hpp for corresponding definitions */
#define SVG_INT_T_STRING "uint16"
#define NPY_SVG_COL NPY_UINT8
#define NPY_SVG_COL_ALT NPY_INT8
#define SVG_COL_T_STRING "uint8 or int8"

/* static global character string for errors */
static char err_msg[1000];

/* actual interface */
#if PY_VERSION_HEX >= 0x03040000 // Py_UNUSED suppress warning from 3.4
static PyObject* multilabel_potrace_svg(PyObject* Py_UNUSED(self),
    PyObject* args, PyObject* kwargs)
{ 
#else
static PyObject* multilabel_potrace_svg(PyObject* self, PyObject* args,
    PyObject* kwargs)
{   (void) self; // suppress unused parameter warning
#endif

    /***  get and check inputs  ***/

    PyArrayObject *py_comp_assign;
    const char* svg_filename;
    double straight_line_tol = 0.5;
    double smoothing = 1.0;
    double curve_fusion_tol = 0.2;
    PyArrayObject *py_comp_colors = NULL;
    PyArrayObject *py_line_color = NULL;
    int line_width = -1;
    int write_raw = 0;
    int write_poly = 0;

    const char* keywords[] = {"", "", "straight_line_tol",
        "smoothing", "curve_fusion_tol", "comp_colors", "line_color",
        "line_width", "write_raw", "write_poly", NULL};
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "Os|dddOOiii",
        (char**) keywords, &py_comp_assign, &svg_filename, &straight_line_tol,
        &smoothing, &curve_fusion_tol, &py_comp_colors, &py_line_color,
        &line_width, &write_raw, &write_poly)){
        return NULL;
    }

    /**  manipulate the raster  **/

    if (!PyArray_Check(py_comp_assign)){
        PyErr_SetString(PyExc_TypeError, "Multilabel Potrace SVG: argument "
            "'comp_assign' must be a numpy array.");
        return NULL;
    }

    if (PyArray_TYPE(py_comp_assign) != NPY_COMP &&
        PyArray_TYPE(py_comp_assign) != NPY_COMP_ALT){
        PyErr_SetString(PyExc_TypeError, "Multilabel Potrace SVG: elements "
            "'comp_assign' must be a of type " COMP_T_STRING ".");
        return NULL;
    }

    if (PyArray_NDIM(py_comp_assign) != 2){
        std::sprintf(err_msg, "Multilabel Potrace SVG: argument 'comp_assign' "
            "must be two-dimensional (%i dimensions given).", 
            PyArray_NDIM(py_comp_assign));
        PyErr_SetString(PyExc_TypeError, err_msg);
        return NULL;
    }

    if (!PyArray_IS_C_CONTIGUOUS(py_comp_assign) &&
        !PyArray_IS_F_CONTIGUOUS(py_comp_assign)){
        PyErr_SetString(PyExc_TypeError, "Multilabel potrace SVG: "
            "'comp_assign' must be contiguous in memory (avoid slicing).");
        return NULL;
    }

    const comp_t* comp_assign = (comp_t*) PyArray_DATA(py_comp_assign);
    const npy_intp* dims = PyArray_DIMS(py_comp_assign);
    bool row_major = PyArray_IS_C_CONTIGUOUS(py_comp_assign);

    if (dims[0] > std::numeric_limits<svg_int_t>::max() ||
        dims[1] > std::numeric_limits<svg_int_t>::max()){
        std::sprintf(err_msg, "Multilabel Potrace SVG: currently, integer "
            "coordinates are represented with " SVG_INT_T_STRING " type, thus"
            "no input dimension can exceed %li (%li-by-%li given)",
            (long) std::numeric_limits<svg_int_t>::max(), dims[0], dims[1]);
        PyErr_SetString(PyExc_ValueError, err_msg);
        return NULL;
    }

    svg_int_t height = dims[0];
    svg_int_t width = dims[1];

    comp_t number_of_components = 0;
    for (size_t i = 0; i < (size_t) width*height; i++){
        if (comp_assign[i] > number_of_components){
            number_of_components = comp_assign[i];
        }
    }
    number_of_components += 1;

    /**  manipulate the file name  **/

    int name_len = std::strlen(svg_filename);
    int base_len = name_len - 4;
    char* base_filename;
    const char* extension = svg_filename + base_len;        
    if (!name_len){
        PyErr_SetString(PyExc_TypeError, "Multilabel Potrace SVG: argument "
            "'comp_assign' must be a nonempty character string.");
        return NULL;
    }else if (base_len <= 0 ||
        (std::strcmp(extension, ".svg") != 0 &&
         std::strcmp(extension, ".SVG") != 0)){
        base_len = name_len;
        name_len = name_len + 4;
        extension = ".svg";
    }
    base_filename = (char*) malloc(base_len + 1);
    std::strncpy(base_filename, svg_filename, base_len);
    base_filename[base_len] = '\0';

    char* raw_filename = NULL;
    char* simplified_filename = NULL;
    char* smoothed_filename = NULL;
    if (!straight_line_tol){
        raw_filename = (char*) malloc(name_len + 1);
        std::strcpy(raw_filename, base_filename);
        std::strcat(raw_filename, extension);
    }else{
        if (write_raw){
            raw_filename = (char*) malloc(name_len + 4 + 1);
            std::strcpy(raw_filename, base_filename);
            std::strcat(raw_filename, "_raw");
            std::strcat(raw_filename, extension);
        }
        if (!smoothing){
            simplified_filename = (char*) malloc(name_len + 1);
            std::strcpy(simplified_filename, base_filename);
            std::strcat(simplified_filename, extension);
        }else{
            if (write_poly){
                simplified_filename = (char*) malloc(name_len + 5 + 1);
                std::strcpy(simplified_filename, base_filename);
                std::strcat(simplified_filename, "_poly");
                std::strcat(simplified_filename, extension);
            }
            smoothed_filename = (char*) malloc(name_len + 1);
            std::strcpy(smoothed_filename, base_filename);
            std::strcat(smoothed_filename, extension);
        }
    }

    /**  check color style  **/

    const svg_color_t* comp_colors = NULL;
    const svg_color_t* line_color = NULL;

    /* PyArg_Parse* does not increase reference count */
    if ((PyObject*) py_comp_colors == Py_None){ py_comp_colors = NULL; }
    if ((PyObject*) py_line_color == Py_None){ py_line_color = NULL; }
    if (line_width == -1){
        if (py_comp_colors && !line_color){ line_width = 0; }
        else { line_width = 1; }
    }else if (line_width <= 0 && !py_comp_colors){
        PyErr_SetString(PyExc_TypeError, "Multilabel potrace SVG: zero line "
            "width and no fill color would render an empty page; check the "
            "requested style.");
        return NULL;
    }

    if (py_comp_colors){
        if (!PyArray_Check(py_comp_colors)){
            PyErr_SetString(PyExc_TypeError, "Multilabel Potrace SVG: argument"
                " 'comp_colors' must be a numpy array.");
            return NULL;
        }

        if (PyArray_TYPE(py_comp_colors) != NPY_SVG_COL &&
            PyArray_TYPE(py_comp_colors) != NPY_SVG_COL_ALT){
            PyErr_SetString(PyExc_TypeError, "Multilabel Potrace SVG: elements"
                " 'comp_colors' must be a of type " SVG_COL_T_STRING ".");
            return NULL;
        }

        if (PyArray_SIZE(py_comp_colors) % 3 != 0){
            std::sprintf(err_msg, "Multilabel Potrace SVG: length of "
                "'comp_colors' must be a multiple of 3 (%li given).",
                PyArray_SIZE(py_comp_colors));
            PyErr_SetString(PyExc_TypeError, err_msg);
            return NULL;
        }else if (PyArray_SIZE(py_comp_colors)/3 != number_of_components){
            std::sprintf(err_msg, "Multilabel Potrace SVG: number of colors in"
                " 'comp_colors' (%li) not equal to the number of components in"
                " 'comp_assign' (%i)", PyArray_SIZE(py_comp_colors)/3,
                number_of_components);
            PyErr_SetString(PyExc_TypeError, err_msg);
            return NULL;
        }

        dims = PyArray_DIMS(py_comp_colors);

        if (PyArray_NDIM(py_comp_colors) == 2){
            if (dims[0] == 3 && dims[1] != 3){
                /* 3-by-N array, must be column-major */
                if (!PyArray_IS_F_CONTIGUOUS(py_comp_colors)){
                    PyErr_SetString(PyExc_TypeError, "Multilabel Potrace SVG: "
                        "internal memory of 'comp_colors' must store each "
                        "color contiguously; a 3-by-N array must be column-"
                        "major (F-contiguous).");
                    return NULL;
                }
            }else if (dims[0] != 3 && dims[1] == 3){
                /* N-by-3 array, must be row-major */
                if (!PyArray_IS_C_CONTIGUOUS(py_comp_colors)){
                    PyErr_SetString(PyExc_TypeError, "Multilabel Potrace SVG: "
                        "internal memory of 'comp_colors' must store each "
                        "color contiguously; a N-by-3 array must be row-major "
                        "(C-contiguous).");
                    return NULL;
                }
            }else if (!PyArray_IS_C_CONTIGUOUS(py_comp_assign) &&
                      !PyArray_IS_F_CONTIGUOUS(py_comp_assign)){
                PyErr_SetString(PyExc_TypeError, "Multilabel potrace SVG: "
                    "'comp_assign' must be contiguous in memory (avoid "
                    "slicing).");
                return NULL;
            }
        } /* else treat as monodimensional, hoping for right memory layout */

        comp_colors = (svg_color_t*) PyArray_DATA(py_comp_colors);
    }

    if (py_line_color){
        if (!PyArray_Check(py_line_color)){
            PyErr_SetString(PyExc_TypeError, "Multilabel Potrace SVG: argument"
                " 'line_color' must be a numpy array.");
            return NULL;
        }

        if (PyArray_TYPE(py_line_color) != NPY_SVG_COL &&
            PyArray_TYPE(py_line_color) != NPY_SVG_COL_ALT){
            PyErr_SetString(PyExc_TypeError, "Multilabel Potrace SVG: elements"
                " 'line_color' must be a of type " SVG_COL_T_STRING ".");
            return NULL;
        }

        if (PyArray_SIZE(py_line_color) != 3){
            std::sprintf(err_msg, "Multilabel Potrace SVG: 'line_color' must "
                " be of length 3 (%li given).", PyArray_SIZE(py_line_color));
            PyErr_SetString(PyExc_TypeError, err_msg);
            return NULL;
        }
        
        line_color = (svg_color_t*) PyArray_DATA(py_line_color);
    }

    /***  call the C++ routine  ***/

    Multi_potrace_svg<comp_t>* mp_svg = new Multi_potrace_svg<comp_t>
            (comp_assign, width, height, number_of_components, row_major);

    mp_svg->set_straight_line_tolerance(straight_line_tol);
    mp_svg->set_smoothing(smoothing, curve_fusion_tol);
    mp_svg->set_style(line_width, line_color, comp_colors);

    mp_svg->trace_and_export(smoothed_filename, simplified_filename,
        raw_filename);

    /* std::cout << *mp_svg << std::endl; // for debugging */

    /***  clean up and return  ***/

    delete mp_svg;

    free(base_filename);
    free(raw_filename);
    free(simplified_filename);
    free(smoothed_filename);

    Py_RETURN_NONE;
}

static const char* documentation = 
"multilabel_potrace_svg(comp_assign, svg_filename, straight_line_tol = 0.5,\n"
"                       smoothing = 1.0, curve_fusion_tol = 0.2,\n"
"                       comp_colors = None, line_color = None,\n"
"                       line_width = 1, write_raw = False,\n"
"                       write_poly = False)\n"
"\n"
"Extract and smooth contours delimiting homogeneous connected components\n"
"within a 2D grid structure (typically, pixels of an image). Resulting lines\n"
"and curves are written in a scalable vector graphics file.\n"
"\n"
"Simplification and smoothing is done by an adaptation of the potrace\n"
"software by Peter Selinger [1] to multilabel rasters (i.e. with more than\n"
"two colors).\n"
"\n"
"NOTA: by default, components are identified using uint16 identifiers;\n"
"this can be changed in the sources if more than 65535 components are\n"
"expected, or if the number of components never exceeds 255 and memory is\n"
"critical (recompilation is necessary)\n"
"\n"
"INPUTS:\n"
"comp_assign - multilabel raster image, assigning a component identifier to\n"
"    each pixel, given as a numpy two-dimensional array of " COMP_T_STRING " elements.\n"
"\n"
"    Components are required to be connected (in the 8-neighbors\n"
"    connectivity sense); a nonconnected component would have several\n"
"    exterior rings (see OUTPUTS) and cause bugs.\n"
"\n"
"    Usually, the component identifiers start at 0, and are sequential up\n"
"    to the highest identifier, but this is not compulsory; each identifier\n"
"    between 0 and the highest not present in the input raster results\n"
"    in an empty path in the output file.\n"
"svg_filename - name of the resulting SVG file, relative or absolute path;\n"
"    if intermediate stages (raw, simplified) are requested (see below)\n"
"    corresponding file names are formed adding suffixes \"raw\" or\n"
"    \"poly\" are added to the file name.\n"
"straight_line_tol - fidelity to the raster: how far (in l-inf distance, \n"
"pixel unit) from a raw border can a straight line approximate it; higher\n"
"    values favor coarse polygons with less line segments; original potrace\n"
"    software is written with this parameter equal to unity; if set to zero,\n"
"    only raw contours are computed and no simplification is performed\n"
"smoothing - potrace alphamax parameter; rougly, from the middle of segments\n"
"    defining a corner, it corresponds to the maximum ratio between the\n"
"    distance to the control points approximating the corner, and the corner\n"
"    point; this quantity is never greater than 4/3, default to unity; if\n"
"    set to zero, no smoothing is performed (only raw contours and\n"
"    simplified polygons are computed)\n"
"curve_fusion_tol - potrace opttolerance parameter; rougly, maximum area of\n"
"    approximation error between simplified polyline and a single curve\n"
"    approximating it; default to .2; if set to zero, no fusion is performed\n"
"    after smoothing\n"
"comp_colors - components colors, numpy array of " SVG_COL_T_STRING ", each\n"
"    color being given by three consecutive RGB coordinates; if N is the\n"
"    the number of components, can thus be a N-by-3 row major (C-contiguous)\n"
"    array or a 3-by-N column major (F-contiguous) array, or unidimensionnal\n"
"    array of length 2E with correct memory layout; set to None (default)\n"
"    for no coloring\n"
"line_color - numpy array of " SVG_COL_T_STRING " of length 3, specifying the\n"
"    RGB coordinates of the color of the lines separating components; None\n"
"    (default) means black if no components colors are provided or if\n"
"    line_width is set explicitely, and no line color otherwise\n"
"line_width - width (in pixel unit) of the lines separating components\n"
"write_raw - if borders simplification is requested (parameter\n"
"    straight_line_tol greater than zero), set this to True for writing\n"
"    also the scale vector graphics of raw borders; corresponding filename\n"
"    deduced from svg_filename with \"raw\" suffix.\n"
"write_poly - if smoothing is requested (parameter smoothing greater than\n"
"    zero), set this to True for writing also the scale vector graphics\n"
"    of simplified polygons borders; corresponding filename deduced from\n"
"    svg_filename with \"poly\" suffix.\n"
"\n"
"Parallel implementation with OpenMP API.\n"
"\n"
"References:\n"
"\n"
"[1] P. Selinger, Potrace: a polygon-based tracing algorithm, 2003,\n"
"http://potrace.sourceforge.net/\n"
"\n"
"Hugo Raguet 2021\n";


static PyMethodDef multilabel_potrace_svg_methods[] = {
    {"multilabel_potrace_svg", (PyCFunction) multilabel_potrace_svg,
        METH_VARARGS | METH_KEYWORDS, documentation},
    {NULL, NULL, 0, NULL}
};

/* module initialization */
static struct PyModuleDef multilabel_potrace_svg_module = {
    PyModuleDef_HEAD_INIT,
    "multilabel_potrace_svg", /* name of module */
    /* module documentation, may be null */
    "wrapper for Multilabel Potrace SVG",
    -1,   /* size of per-interpreter state of the module,
             or -1 if the module keeps state in global variables. */
    multilabel_potrace_svg_methods, /* actual methods in the module */
    NULL, /* multi-phase initialization, may be null */
    NULL, /* traversal function, may be null */
    NULL, /* clearing function, may be null */
    NULL  /* freeing function, may be null */
};

PyMODINIT_FUNC
PyInit_multilabel_potrace_svg(void)
{
    import_array() /* IMPORTANT: this must be called to use numpy array */

    PyObject* m;

    /* create the module */
    m = PyModule_Create(&multilabel_potrace_svg_module);
    if (!m){ return NULL; }

    return m;
}
