/*=============================================================================
 * Python extension module for Multilabel Potrace Graphs
 * 
 * Hugo Raguet 2023
 *===========================================================================*/
#include <cstdint>
#include <limits>
#include <cstdio>
#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#include "multilabel_potrace.hpp"

/* type of components identifiers */
typedef uint16_t comp_t; // we do not expect more than 65535 components
#define NPY_COMP NPY_UINT16
#define NPY_COMP_ALT NPY_INT16 // can be safely cast as NPY_COMP
#define COMP_T_STRING "uint16 or int16"

/* type for components adjacency edge identifiers */
typedef uint32_t index_t;
#define NPY_ADJ_IND NPY_UINT32
#define NPY_ADJ_IND_ALT NPY_INT32 // can be safely cast as NPY_ADJ_IND
#define ADJ_IND_T_STRING "uint32 or int32"

/* type for integer coordinates */
typedef uint16_t int_coor_t; // no dimension larger than 65535
#define INT_COOR_T_STRING "uint16"

/* type for output: real coordinates, indices */
typedef double real_coor_t;
#define NPY_POLYGRAPH_REAL NPY_FLOAT64
#define PG_REAL_T_STRING "float64"
#define NPY_POLYGRAPH_INT NPY_UINT32
#define PG_INT_T_STRING "uint32"

/* specialize some types */
typedef typename Multi_potrace<comp_t, int_coor_t, real_coor_t>::Point_r
    Point_r;
typedef typename Multi_potrace<comp_t, int_coor_t, real_coor_t>::
    template Path<Point_r> Path_r;
typedef typename Multi_potrace<comp_t, int_coor_t, real_coor_t>::
    template Border<Point_r> Border_r;
typedef typename Multi_potrace<comp_t, int_coor_t, real_coor_t>::
    template Contour<Point_r> Contour_r;

/**  create the `Potrace polygraph` type as a struct sequence type,
 *   aka named tuple  **/

static PyStructSequence_Field Potrace_polygraph_fields[] = {
    {"vertices",
     "numpy float array of shape 2-by-number_of_vertices; the 2D coordinates\n"
     " of the vertices."},
    {"edges",
     "numpy integer array of shape 2-by-number_of_edges; for each edge, the \n"
     "indices of its endpoints in the array 'vertices'\n"},
    {"graph_indices",
     "numpy integer array of shape 2-by-number_of_vertices; for each vertex,\n"
     " the index of the local graph it belongs to"},
    {NULL}
};

static PyStructSequence_Desc Potrace_polygraph_desc = {
    "Potrace polygraph", /* name */
    "The data structure of the polygons or polylines represented as graphs \n"
    "is a python \"named tuple\", with the following entries: 'vertices', \n"
    "'edges' and 'graph_indices'.", /* docstring */
    Potrace_polygraph_fields, /* fields */
    3, /* number of fields visible to the Python side (if used as tuple) */
};

/* static global object; initialized and passed to the module at module init */
static PyTypeObject Potrace_polygraph;

/* static global character string for errors */
static char err_msg[1000];

/* actual interface */
static PyObject* multilabel_potrace_polygraph(PyObject* Py_UNUSED(self),
    PyObject* args, PyObject* kwargs)
{
    /***  get and check inputs  ***/
    PyArrayObject *py_comp_assign, *py_first_edge, *py_adj_components;
    py_first_edge = py_adj_components = NULL;
    double straight_line_tol = 0.5;

    const char* keywords[] = {"", "first_edge", "adj_components",
        "straight_line_tol", NULL};
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O|OOd", (char**) keywords,
        &py_comp_assign, &py_first_edge, &py_adj_components,
        &straight_line_tol)){
        return NULL;
    }

    /* input check on comp_assign; get also dimensions and max number of
     * components */

    if (!PyArray_Check(py_comp_assign)){
        PyErr_SetString(PyExc_TypeError, "Multilabel potrace Polygraph: "
            "argument 'comp_assign' must be a numpy array.");
        return NULL;
    }

    if (PyArray_TYPE(py_comp_assign) != NPY_COMP &&
        PyArray_TYPE(py_comp_assign) != NPY_COMP_ALT){
        PyErr_SetString(PyExc_TypeError, "Multilabel potrace Polygraph: "
            "elements of 'comp_assign' must be of type " COMP_T_STRING ".");
        return NULL;
    }

    if (PyArray_NDIM(py_comp_assign) != 2){
        std::sprintf(err_msg, "Multilabel potrace Polygraph: argument "
            "'comp_assign' must be two-dimensional (%i dimensions given).", 
            PyArray_NDIM(py_comp_assign));
        PyErr_SetString(PyExc_TypeError, err_msg);
        return NULL;
    }

    if (!PyArray_IS_C_CONTIGUOUS(py_comp_assign) &&
        !PyArray_IS_F_CONTIGUOUS(py_comp_assign)){
        PyErr_SetString(PyExc_TypeError, "Multilabel potrace Polygraph: "
            "'comp_assign' must be contiguous in memory (avoid slicing).");
        return NULL;
    }

    const comp_t* comp_assign = (comp_t*) PyArray_DATA(py_comp_assign);
    const npy_intp* dims = PyArray_DIMS(py_comp_assign);
    bool row_major = PyArray_IS_C_CONTIGUOUS(py_comp_assign);

    if (dims[0] > std::numeric_limits<int_coor_t>::max() ||
        dims[1] > std::numeric_limits<int_coor_t>::max()){
        std::sprintf(err_msg, "Multilabel potrace Polygraph: currently, "
            "integer coordinates are represented with " INT_COOR_T_STRING
            " type, thus no input dimension can exceed %li (%li-by-%li given)",
            (long) std::numeric_limits<int_coor_t>::max(), dims[0], dims[1]);
            PyErr_SetString(PyExc_ValueError, err_msg);
        return NULL;
    }

    int_coor_t height = dims[0];
    int_coor_t width = dims[1];

    comp_t number_of_components = 0;
    for (size_t i = 0; i < (size_t) width*height; i++){
        if (comp_assign[i] > number_of_components){
            number_of_components = comp_assign[i];
        }
    }
    number_of_components += 1;

    const index_t* first_edge = NULL;
    const comp_t* adj_components = NULL;

    if (py_first_edge){ /* input check on optional connectivity */

    if (!(PyArray_Check(py_first_edge) && PyArray_Check(py_adj_components))){
        PyErr_SetString(PyExc_TypeError, "Multilabel potrace Polygraph: "
            "arguments 'first_edge' and 'py_adj_components' must be a numpy "
            "arrays.");
        return NULL;
    }

    if (PyArray_TYPE(py_first_edge) != NPY_ADJ_IND &&
        PyArray_TYPE(py_first_edge) != NPY_ADJ_IND_ALT){
        PyErr_SetString(PyExc_TypeError, "Multilabel potrace Polygraph: "
            "elements of 'first_edge' must be of type " ADJ_IND_T_STRING ".");
        return NULL;
    }

    if (PyArray_TYPE(py_adj_components) != NPY_COMP &&
        PyArray_TYPE(py_adj_components) != NPY_COMP_ALT){
        PyErr_SetString(PyExc_TypeError, "Multilabel potrace Polygraph: "
            "elements of 'adj_components' must be of type " COMP_T_STRING ".");
        return NULL;
    }

    if (PyArray_SIZE(py_first_edge) != number_of_components + 1){
        std::sprintf(err_msg, "Multilabel potrace Polygraph: argument "
            "'first_edge' should contain %i elements (one more than the "
            "number of components), but %li are given.", 
            number_of_components + 1, PyArray_SIZE(py_first_edge));
        PyErr_SetString(PyExc_TypeError, err_msg);
        return NULL;
    }

    first_edge = (index_t*) PyArray_DATA(py_first_edge);
    adj_components = (comp_t*) PyArray_DATA(py_adj_components);

    } /* end input check on optional connectivity */

    /***  process the raster  ***/

    Multi_potrace<comp_t, int_coor_t, real_coor_t>* multi_potrace =
        new Multi_potrace<comp_t, int_coor_t, real_coor_t>
            (comp_assign, width, height, number_of_components, row_major);

    multi_potrace->set_straight_line_tolerance(straight_line_tol);
    multi_potrace->set_smoothing(0.0, 0.0); // polygons only
    multi_potrace->trace();

    /* std::cout.precision(2);
    std::cout << *multi_potrace << std::endl; // for debugging */

    /***  create and fill outputs
     * WARNING: no check for successful allocations is performed  ***/
    npy_intp size_py_vertices[] = {2, 0};
    PyArrayObject* py_vertices;
    real_coor_t* vertices;
    npy_intp size_py_edges[] = {2, 0};
    PyArrayObject* py_edges;
    index_t* edges;
    npy_intp size_py_graph_ind[] = {0};
    PyArrayObject* py_graph_ind;
    index_t* graph_indices;

    PyObject *py_components, *py_interfaces;

    /**  polygons for the components  **/

    /* first pass: compute total number of points and edges */
    index_t number_of_vertices = 0;
    for (comp_t comp = 0; comp < number_of_components; comp++){
        const Contour_r& contour = multi_potrace->simplified_contours[comp];
        for (int i = 0; i < contour.number_of_borders; i++){
            const Border_r& border = contour.borders[i];
            for (int j = 0; j < border.number_of_paths; j++){
                const Path_r& path = border.paths[j];
                number_of_vertices += path.length;
            }
        }
    }
    index_t number_of_edges = number_of_vertices; /* closed polylines */

    /* create arrays for polygraph structure */
    size_py_vertices[1] = number_of_vertices;
    py_vertices = (PyArrayObject*) PyArray_ZEROS(2, size_py_vertices,
        NPY_POLYGRAPH_REAL, 1);
    vertices = (real_coor_t*) PyArray_DATA(py_vertices);

    size_py_edges[1] = number_of_edges;
    py_edges = (PyArrayObject*) PyArray_ZEROS(2, size_py_edges,
        NPY_POLYGRAPH_INT, 1);
    edges = (index_t*) PyArray_DATA(py_edges);

    size_py_graph_ind[0] = number_of_vertices;
    py_graph_ind = (PyArrayObject*) PyArray_ZEROS(1, size_py_graph_ind,
        NPY_POLYGRAPH_INT, 1);
    graph_indices = (index_t*) PyArray_DATA(py_graph_ind);

    /* second pass: fill the polygraph structure */
    index_t point_index = 0;
    for (comp_t comp = 0; comp < number_of_components; comp++){
        const Contour_r& contour = multi_potrace->simplified_contours[comp];
        for (int i = 0; i < contour.number_of_borders; i++){
            const Border_r& border = contour.borders[i];
            index_t first_point_index = point_index;
            for (int j = 0; j < border.number_of_paths; j++){
                const Path_r& path = border.paths[j];
                const bool is_reverse = comp > path.other_comp;
                for (int k = 0; k < path.length; k++){
                    const Point_r& point = is_reverse ?
                        path.segments[path.length - k] : path.segments[k];
                    vertices[2*point_index] = point.x;
                    vertices[2*point_index + 1] = point.y;
                    edges[2*point_index] = point_index;
                    edges[2*point_index + 1] = point_index + 1; // see below
                    graph_indices[point_index] = comp;
                    point_index++;
                }
            } /* border complete */
            /* last edge should connect the last point to the first */
            edges[2*point_index - 1] = first_point_index;
        }
    }

    /* create and fill the Potrace polygraph named tuple */
    py_components = PyStructSequence_New(&Potrace_polygraph);
    PyStructSequence_SET_ITEM(py_components, 0, (PyObject*) py_vertices);
    PyStructSequence_SET_ITEM(py_components, 1, (PyObject*) py_edges);
    PyStructSequence_SET_ITEM(py_components, 2, (PyObject*) py_graph_ind);

    if (first_edge){ /**  polylines for the interfaces  **/

        /* first pass: compute total number of points and edges */
        number_of_vertices = 0;
        index_t number_of_edges = 0; 
        /* iterate over components */
        for (comp_t comp = 0; comp < number_of_components; comp++){
            const Contour_r& contour =
                multi_potrace->simplified_contours[comp];

            /* iterate over adjacent components */
            for (index_t e = first_edge[comp]; e < first_edge[comp + 1]; e++){
                comp_t other_comp = adj_components[e];

    /* look in all paths of all borders of the component for an interface with
     * the considered other component; there must be at least one such path,
     * but there can be several; currently, all paths are considered
     * disconnected, so that they are either circular, in which case the last
     * point needs not be created, or they are not, in which case case the last
     * point must be created */
                for (int i = 0; i < contour.number_of_borders; i++){
                    const Border_r& border = contour.borders[i];
                    for (int j = 0; j < border.number_of_paths; j++){
                        const Path_r& path = border.paths[j];
                        if (path.other_comp != other_comp){ continue; }
                        number_of_edges += path.length;
                        number_of_vertices += path.length;
                        if (path.first_point() != path.last_point()){
                            number_of_vertices++;
                        }
                    } /* border complete */
                } /* contour complete */

           } /* end scanning adjacent components  */
        } /* end scanning all components */

        /* create and fill arrays for polygraph structure */
        size_py_vertices[1] = number_of_vertices;
        py_vertices = (PyArrayObject*) PyArray_ZEROS(2, size_py_vertices,
            NPY_POLYGRAPH_REAL, 1);
        vertices = (real_coor_t*) PyArray_DATA(py_vertices);

        size_py_edges[1] = number_of_edges;
        py_edges = (PyArrayObject*) PyArray_ZEROS(2, size_py_edges,
            NPY_POLYGRAPH_INT, 1);
        edges = (index_t*) PyArray_DATA(py_edges);

        size_py_graph_ind[0] = number_of_vertices;
        py_graph_ind = (PyArrayObject*) PyArray_ZEROS(1, size_py_graph_ind,
            NPY_POLYGRAPH_INT, 1);
        graph_indices = (index_t*) PyArray_DATA(py_graph_ind);

        /* second pass: fill the polygraph structure */
        point_index = 0; 
        index_t edge_index = 0; 
        index_t interface_index = 0;
        /* iterate over components */
        for (comp_t comp = 0; comp < number_of_components; comp++){
            const Contour_r& contour =
                multi_potrace->simplified_contours[comp];

            /* iterate over adjacent components */
            for (index_t e = first_edge[comp]; e < first_edge[comp + 1]; e++){
                comp_t other_comp = adj_components[e];

                /* look in all paths of all borders of the component for an
                 * interface with the considered other component;
                 * see above comments */
                for (int i = 0; i < contour.number_of_borders; i++){
                    const Border_r& border = contour.borders[i];
                    for (int j = 0; j < border.number_of_paths; j++){
                        const Path_r& path = border.paths[j];
                        if (path.other_comp != other_comp){ continue; }

                        const bool is_reverse = comp > path.other_comp;
                        const int first_point_index = point_index;

                        for (int k = 0; k < path.length; k++){
                            const Point_r& point = is_reverse ?
                                path.segments[path.length - k] :
                                path.segments[k];
                            vertices[2*point_index] = point.x;
                            vertices[2*point_index + 1] = point.y;
                            graph_indices[point_index] = interface_index;
                            edges[2*edge_index] = point_index;
                            edges[2*edge_index + 1] = point_index + 1;
                            point_index++;
                            edge_index++;
                        }

                        if (path.first_point() != path.last_point()){
                            const Point_r& point = is_reverse ?
                                path.first_point() : path.last_point();
                            vertices[2*point_index] = point.x;
                            vertices[2*point_index + 1] = point.y;
                            graph_indices[point_index] = interface_index;
                            point_index++;
                        }else{
                            edges[2*edge_index - 1] = first_point_index;
                        }

                    } /* border complete */
                } /* contour complete */
                interface_index++;

           } /* end scanning adjacent components  */
        } /* end scanning all components */

        /* create and fill the Potrace polygraph named tuple */
        py_interfaces = PyStructSequence_New(&Potrace_polygraph);
        PyStructSequence_SET_ITEM(py_interfaces, 0, (PyObject*) py_vertices);
        PyStructSequence_SET_ITEM(py_interfaces, 1, (PyObject*) py_edges);
        PyStructSequence_SET_ITEM(py_interfaces, 2, (PyObject*) py_graph_ind);

    } /* endif first_edge */

    /***  clean up and return; all PyObject references have been passed
     *    except components and interfaces  ***/
    delete multi_potrace;

    if (first_edge){
        PyObject* py_Out = PyTuple_New(2);
        PyTuple_SET_ITEM(py_Out, 0, py_components);
        PyTuple_SET_ITEM(py_Out, 1, py_interfaces);
        return py_Out;
    }else{
        return py_components;
    }

}

static const char* documentation = 
"components, [interfaces] = multilabel_potrace_polygraph(comp_assign, \n"
"   first_edge = None, adj_components = None, straight_line_tol = 0.5)\n"
"\n"
"Extract and simplifies contours delimiting homogeneous connected components\n"
"within a 2D grid structure (typically, pixels of an image). Resulting\n"
"polygons are represented as graphs. Additionaly, interfaces between \n"
"adjacent components can be extracted as polylines, also represented as \n"
"graphs (this requires to specify components adjacency as input).\n"
"\n"
"Simplifications is done by an adaptation of the potrace software by Peter\n"
"Selinger [1] to multilabel rasters (i.e. with more than two colors).\n"
"\n"
"NOTA: by default, components are identified using uint16 identifiers;\n"
"this can be changed in the sources if more than 65535 components are\n"
"expected, or if the number of components never exceeds 255 and memory is\n"
"critical (recompilation is necessary)\n"
"\n"
"INPUTS:\n"
"comp_assign - multilabel raster image, assigning a component identifier to\n"
"    each pixel, given as a numpy two-dimensional array of " COMP_T_STRING "\n"
"    elements.\n"
"\n"
"    Note that if a components are not connected (in the 8-neighbors\n"
"    connectivity sense) it will result in a polygon polygon with several \n"
"    exterior boundaries.\n"
"\n"
"    Usually, the component identifiers start at 0, and are sequential up\n"
"    to the highest identifier, but this is not compulsory; each identifier\n"
"    between 0 and the highest not present in the input raster results\n"
"    in an empty polygon at the corresponding index in the output list.\n"
"first_edge, adj_components - specification of components connectivity as\n"
"    forward-star graph representation :\n"
"    components are numeroted as in comp_assign;\n"
"    edges are numeroted (start at 0) so that all edges originating from a\n"
"        same components are consecutive;\n"
"    for each components, 'first_edge' indicates the first edge starting\n" 
"        from the component (or, if none, starting from the next one);\n"
"        (" ADJ_IND_T_STRING ") array of length number of components plus\n"
"        one, the last value being the total number of edges;\n"
"    for each edge, 'adj_components' indicates the other component, ("
"        (" COMP_T_STRING ")\n"
"straight_line_tol - fidelity to the raster: how far (in l-inf distance, \n"
"pixel unit) from a raw border can a straight line approximate it; higher\n"
"    values favor coarse polygons with less line segments.\n"
"\n"
"OUTPUTS:\n"
"components - a python \"named tuple\" specifying the polygons delimiting\n"
"    the components, given as linear graphs; with the following entries:\n"
"    - 'vertices': numpy float array of shape 2-by-number_of_vertices, with\n"
"       the 2D coordinates of the vertices\n"
"    - 'edges': numpy integer array of shape 2-by-number_of_edges, with for\n"
"       each edge, the indices of its endpoints in the array 'vertices'\n"
"    - 'graph_indices': numpy integer array 2-by-number_of_vertices\n, with"
"       for each vertex, the index of the local graph it belongs to\n"
"interfaces - similar structure as above, specifying the polylines\n"
"    constituing interfaces between adjacent components as linear graphs;\n"
"    requires parameters 'first_edge' and 'adj_components' specifying\n"
"    components adjacency; identifiers in 'graph_indices' thus corresponds\n"
"    to the order in which adjacency of components are given\n"
"\n"
"As usual in planar coordinate system but in contrast to matrix indexing,\n"
"the origin is put at the lower-left corner of the raster, the x-axis grows\n"
"left-to-right, and the y-axis grows bottom-to-top. Base unit sizes are the\n"
"pixels sides, so that the corners of the pixels have integer coordinates.\n"
"\n"
"Parallel implementation with OpenMP API.\n"
"\n"
"References:\n"
"\n"
"[1] P. Selinger, Potrace: a polygon-based tracing algorithm, 2003,\n"
"http://potrace.sourceforge.net/\n"
"\n"
"Hugo Raguet 2023\n";

static PyMethodDef multilabel_potrace_polygraph_methods[] = {
    {"multilabel_potrace_polygraph",
        (PyCFunction) multilabel_potrace_polygraph,
        METH_VARARGS | METH_KEYWORDS, documentation},
    {NULL, NULL, 0, NULL}
};

/* module initialization */
static struct PyModuleDef multilabel_potrace_polygraph_module = {
    PyModuleDef_HEAD_INIT,
    "multilabel_potrace_polygraph", /* name of module */
    /* module documentation, may be null */
    "wrapper for Multilabel Potrace, with special named tuple type\n"
    "\"Potrace polygraph\" for specifying the resulting polygons constituing\n"
    " the components and polylines constituing their interfaces, as linear \n"
    "graphs.\n",
    -1,   /* size of per-interpreter state of the module,
             or -1 if the module keeps state in global variables. */
    multilabel_potrace_polygraph_methods, /* actual methods in the module */
    NULL, /* multi-phase initialization, may be null */
    NULL, /* traversal function, may be null */
    NULL, /* clearing function, may be null */
    NULL  /* freeing function, may be null */
};

PyMODINIT_FUNC
PyInit_multilabel_potrace_polygraph(void)
{
    import_array() /* IMPORTANT: this must be called to use numpy array */

    PyObject* m;

    /* create the module */
    m = PyModule_Create(&multilabel_potrace_polygraph_module);
    if (!m){ return NULL; }

    /* add the Potrace polygraph struct sequence to the module */
    PyStructSequence_InitType(&Potrace_polygraph, &Potrace_polygraph_desc);
    Py_INCREF(&Potrace_polygraph);
    if (PyModule_AddObject(m, "Potrace_polygraph",
        (PyObject*) &Potrace_polygraph) < 0) {
        Py_DECREF(&Potrace_polygraph);
        Py_DECREF(m);
        return NULL;
    }

    return m;
}
